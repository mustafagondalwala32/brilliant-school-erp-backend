module.exports = (function () {
    this.addFileToS3 = async ({decoded,files}) => {
        const {file} = files
        const {schoolId,yearId} = decoded
        const uploadedFile = await ImageUpload(file,schoolId,yearId)
        return {
            data:{
                uploadedFile
            }
        }
    }
    this.removeFileFromS3 = async ({body,decoded}) => {
        return {decoded}
    }
    return this;
  })();