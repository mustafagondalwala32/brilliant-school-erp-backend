const School = require('../../models/school')
const Student = require('../../models/student')
const Class = require('../../models/class')
const Teacher = require('../../models/teacher')
const FeeCategory = require('../../models/feeCategory')
const FeeInstallment = require('../../models/feeInstallment')
const ClassWiseFee = require('../../models/classWiseFee')
const StudentWiseFee = require('../../models/studentWiseFees')



module.exports = (function () {

    /**
     * Create new School
     * @param {*} param0 
     */
    this.createNewFeeCategory = async({body,decoded}) => {
        const {fee_category_name} = body
        const {schoolId,yearId} = decoded
        const isExist = await FeeCategory.find({schoolId,yearId,categoryName:fee_category_name}).countDocuments()
        if(isExist != 0){
            TE("Fee Category is Already Exists")
        }
        const newFeeCategory = await FeeCategory.create({schoolId,yearId,categoryName:fee_category_name})

        return {
            data:{
                message:"New Fee Category has added",
                newFeeCategory,
                total:await FeeCategory.find({schoolId,yearId}).countDocuments()       
            }
        }

    }


    this.createNewFeeInstallment = async({body,decoded}) => {
        const {installment_name} = body
        const {schoolId,yearId} = decoded
        const isExist = await FeeInstallment.find({schoolId,yearId,installmentName:installment_name}).countDocuments()
        if(isExist != 0){
            TE("Fee Installment is Already Exists")
        }
        const newFeeInstallment = await FeeInstallment.create({schoolId,yearId,installmentName:installment_name})

        return {
            data:{
                message:"New Fee Installment has added",
                newFeeInstallment,
                total:await FeeInstallment.find({schoolId,yearId}).countDocuments()       
            }
        }
    }

    this.getAllFeeCategory = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {skip,limit} = body
        const feeCategory = await FeeCategory.find({schoolId,yearId}).skip(skip).limit(limit)
        return {
            data:{
                feeCategory,
                totalPage:feeCategory.length,
                total:await FeeCategory.find({schoolId,yearId}).countDocuments(),
                pagination:{
                    skip,limit
                }
            }
        }
    }

    this.getAllFeeInstallment = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {skip,limit} = body
        const feeInstallment = await FeeInstallment.find({schoolId,yearId}).skip(skip).limit(limit)
        return {
            data:{
                feeInstallment,
                totalPage:feeInstallment.length,
                total:await FeeInstallment.find({schoolId,yearId}).countDocuments(),
                pagination:{
                    skip,limit
                }
            }
        }
    }

    this.updateFeeCategory = async({body,decoded}) => {
        const {feeCategoryId,fee_category_name} = body;
        const feeCategory = await FeeCategory.findById(feeCategoryId);
        if(feeCategory == null){
            TE("Cannot find FeeCategory")
        }
        feeCategory.name = fee_category_name

        await feeCategory.save()

        return {
            data:{
                message:"Fee Category Updated",
                feeCategory
            }
        }
    }

    this.updateFeeInstallment = async ({body,decoded}) => {
        const {installment_name,installmentId} = body
        const feeInstallment = await FeeInstallment.findById(installmentId)
        if(feeInstallment == null) {
            TE("Cannot Find Fee Installment")
        }
        feeInstallment.name = installment_name
        await feeInstallment.save()
        return {
            data:{
                message:"Fee Installment Updated",
                feeInstallment
            }
        }

    }
    this.deleteFeeCategory = async({body,decoded,params}) => {
        const {feeCategoryId} = params
        const feeCategoryDelete = await FeeCategory.findByIdAndDelete(feeCategoryId);
        return {
            data:{
                message:"Fee Category Deleted",
                feeCategoryDelete
            }
        }
    }
    this.deleteFeeInstallment = async({body,decoded,params}) => {
        const {feeInstallmentId} = params
        const feeInstallmentDelete = await FeeInstallment.findByIdAndDelete(feeInstallmentId);
        return {
            data:{
                message:"Fee Installment Deleted",
                feeInstallmentDelete
            }
        }
    }
    this.updateClassWiseFeeInstallment = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {classId,feeStructure} = body
        var classInstance = await ClassWiseFee.findOne({schoolId,yearId,classId})
        if(classInstance == null) {
            classInstance = new ClassWiseFee({schoolId,yearId,classId})
        }
        classInstance.feeStructure = feeStructure
        await classInstance.save();
        return {
            data:{
                message:"Class wise fee structure updated",
            }
        }
    }
    this.getClassWiseFeeInstallment = async({body,decoded}) => {
        const {school,yearId} = decoded
        const {classId} = body
        if(!classId || !verifyObjectId(classId)) {
            TE("Invalid Class Id")
        }
        const classwiseFeeStructure = await ClassWiseFee.findOne({school,yearId,classId}).populate('feeStructure.installmentId').populate('feeStructure.categoryWise.categoryId')
        return {
            data:{
                classwiseFeeStructure
            }
        }
    }
    this.addFeeToStudent = async ({body,decoded}) => {
        const {classId} = body
        const {yearId,schoolId} = decoded

        // get the fee Structuce of fees
        const classFeeStructuce = await ClassWiseFee.findOne({yearId,schoolId,classId})
        if(classFeeStructuce == null){
            TE("Cannot Find the Fee Structuce")
        }

        // get all student of class
        const all_student = await Student.find({yearId,schoolId,classId}).select('_id')
        const session = await StudentWiseFee.startSession();
        session.startTransaction();
        try{

        const saveArray = []
        var isExist;
        await Promise.all(
            await all_student.map(async student => {

                isExist = await StudentWiseFee.findOne({ studentId: student._id,schoolId,yearId,classId})
                if(isExist == null){
                    saveArray.push({
                        studentId: student._id,
                        schoolId,
                        yearId,
                        classId,
                        feeStructure:classFeeStructuce.feeStructure
                    })
                }
            })
        )
        
        await StudentWiseFee.insertMany(saveArray)

    }catch(error){
            await session.abortTransaction();
            session.endSession();
            throw error; 
        }     
        return {
            data:{
                message:"Fee Added to all student",
                total:await StudentWiseFee.find({schoolId,yearId,classId}).countDocuments()
            }
        }
    }
    this.updateStudentFee = async({body,decoded}) => {
        const {schoolId,yearId} = decoded;
        const {studentId,feeStructure} = body
        var studentfee;
        studentfee = await StudentWiseFee.findOne({studentId,schoolId,yearId})
        if(studentfee == null){
            var studentClass = await Student.findByIdAndDelete(studentId)
            studentfee = new StudentWiseFee({studentId,schoolId,yearId,classId:studentClass.classId})
        }

        studentfee.feeStructure = feeStructure
        await studentfee.save()
        return {
            data:{
                message:"Student Fee Updated"
            }
        }


    }
    return this;
  })();