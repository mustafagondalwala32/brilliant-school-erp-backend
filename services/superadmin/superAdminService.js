const SuperAdmin = require('../../models/superadmin')
const jwt = require("jsonwebtoken");
const crypto = require('crypto');
module.exports = (function () {
    /**
     * @params body
     * @For login the superAdmin
     */
    this.loginSuperAdmin = async({body}) => {
        // get the values
        const {email,password} = body

        // if if email is avaible
        const checkSuperAdmin = await SuperAdmin.findOne({adminEmail:email})
        // check if credient are correct
        if (!checkSuperAdmin || !bcrypt.compareSync(password, checkSuperAdmin.hash)) {
            throw new Error("Invalid email or password")
        }    

        const token = jwt.sign({ _id: checkSuperAdmin._id }, process.env.LOGIN_SUPERADMIN_SECRETE, {
            expiresIn: "3649635 days"
          });
        
        // update the superadmin with the loginToken
        await checkSuperAdmin.updateOne({loginToken:token})

        // get the superadmin instance from the database
        const superadmin = await SuperAdmin.findById(checkSuperAdmin._id)
        return {data:{superadmin,token,"message":"Login successfull"}}
    }
    

    /**
     * @params body
     * @For get all admin in system
     */
    this.getAllAdmin = async({body}) => {
        const {skip,limit} = body;
        const admins = await Admin.find().select("adminEmail adminName createdAt updatedAt").skip(skip).limit(limit)
        return {data:{admins}}
    }

    /**
     * 
     * @param {email} superadmin's email 
     * @param {name} superadmin's name 
     * @param {password} superadmin's password 
     * 
     * @For  create new superadmin in system (only for development environment)
     */
    this.createSuperAdmin = async({body}) => {

        // get all values
        const {name,email,password} = body
        if(!name || !email || !password){
            TE("Invalid parameter: email,name,password is required for create superadmin")
        }

        let isExist = await SuperAdmin.find({adminEmail:email}).countDocuments()
        if(isExist != 0){
            TE("SuperAdmin email already exists. Please try another one")
        }

        const newSuperAdmin = new SuperAdmin({
            adminEmail:email,
            adminName:name,
            hash:bcrypt.hashSync(password, 10)
        })
        await newSuperAdmin.save()
        if(newSuperAdmin == null){
            TE("Error occurred while creating superadmin")
        }
        return {
            data:{
                message:"Super Admin created successfully",
                newSuperAdmin
            }
        }
    }

    /**
     * @params body
     * @For update super admin details
     */
    this.updateSuperAdmin = async({body,decoded}) => {
        // get the super admin _id
        console.log(decoded,body)
        const superUserId = decoded._id
        // get the update details
        const {name,email,password} = body
        // get the super admin instance
        const superadmin = await SuperAdmin.findById(superUserId)
        if(!superadmin){
            TE("Cannot find super admin")
        }

        if(name){
            superadmin.adminName = name
        }

        if(email){
            superadmin.adminEmail = email
        }

        if(password){
            superadmin.hash = bcrypt.hashSync(password, 10)
            superadmin.loginToken = ""
        }
        await superadmin.save()

        const updatedAdmin = await SuperAdmin.findById(superUserId)
        return {data:{updatedAdmin,"message":"Admin Updated!!"}}
    }


    /**
     * @For showing all the super users
     */
    this.getAllSuperAdmin = async ({body}) => {
        const {skip,limit} = body
        const superadmins = await SuperAdmin.find().skip(skip).limit(limit)
        return {
            data:{superadmins,"total":superadmins.length}
        }
    }
    /**
     * @params body
     * @For logout super admin
     */
    this.logoutSuperAdmin = async({decoded}) => {
        // get varibles
        const superadminId = decoded._id
        // check if superadmin is available
        if(!superadminId) {
            throw new Error("Invalid superAdminId")
        }

        const superadmin = await Admin.findById(superadminId)
        if(!superadmin){
            throw new Error("Cannot find superadmin with id " + superadminId)
        }
        // update the loginToken is empty string
        superadmin.loginToken = ""
        superadmin.save()
        return {data:{"message":"user loggout"}}
    }


    /**
     * @params {email} superadmin email
     * @For generate the resettoken for reset password
     */
    this.forgetPassword = async({body}) => {
        // get email from body
      const {email} = body

      // check if email is available
      if(!email) TE("Cannot Found the email.")

      // get the admin instance from the database
      const adminInstance = await SuperAdmin.findOne({adminEmail: email})


      // check if admin email is available in database
      if(adminInstance == null) TE("Cannot Found superadmin in db")


      // token
      let token = generateRandomString(100)

      // update the adminInstance with resetPasswordToken
      adminInstance.resetPasswordToken = token;
      adminInstance.resetPasswordExpires = Date.now() + 3600000;
      adminInstance.loginToken = undefined
      

      let resetLink = process.env.HOST + "/api/admin/reset-password?resetToken"+token

      // 1 hour
      await adminInstance.save()

      // send the link to email
      return {
        data:{
          message:"Reset Link Sended",
          resetToken:token,
        }
      }
    }


    /**
     * @params {resetToken} token sended with email
     * @For generate the new superadmin password
     */
    this.resetPassword = async({body}) => {
        // get all the values from body
        const {resetToken,password} = body
  
        // check if all exists
        if(!resetToken || !password) TE("Invalid Parameter: resetToken and password required.")
  
        // get the admin instance from the database
        const adminInstance = await SuperAdmin.findOne({ resetPasswordToken: resetToken, resetPasswordExpires: { $gt: Date.now() } })
        
        // if admin instance is null than throw error
        if(adminInstance === null) TE("Password reset token is invalid or has expired")
  
  
        // update the admin instance
        adminInstance.resetPasswordToken = undefined;
        adminInstance.resetPasswordExpires = undefined;
        adminInstance.loginToken = undefined
        adminInstance.hash = bcrypt.hashSync(password, 10)
  
        // update the admin instance to database
        return {
          data:{
            message:"Reset Password successfull",
            adminInstance:await adminInstance.save()
          }
        }
      }

    return this;
  })();