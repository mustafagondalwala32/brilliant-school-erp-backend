const School = require('../../models/school')
const StudentDocument = require('../../models/studentDocument')
const Student = require('../../models/student')
const Teacher = require('../../models/teacher')
const TeacherDocument = require('../../models/teacherDocument')
const User = require('../../models/user')
const Class = require('../../models/class')
const RegisterStudent = require('../../models/registerStudent')
const mongoose = require('mongoose')


module.exports = (function () {
    this.createNewTeacher = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {empid,firstname,lastName,middleName,email,gender,phoneNumber,address,qualification,dob,bloodGroup,dateOfJoining,aadharCard,bankName,bankNo,pandCardNo,teacherPhoto,experienceLetter,idProof,otherDocuments1,otherDocuments2,salary_amount,pfNo,pfAmount,tdsAmount,professionalTaxAmount,daAmount,hraAmount,salaryRemark,cascualLeave,payEarnLeave,sickLeave,otherLeave} = body;
        
        if(!empid || !firstname || !lastName || !gender || !phoneNumber || !salary_amount){
            TE("Invalid Parameter: empid,firstname,lastName,gender,phoneNumber,salary_amount is required")
        }
        const isExist = await Teacher.find({yearId,schoolId,empid}).countDocuments()
        if(isExist > 0){
            TE("Teacher Already Exists. Please try another EmpId")
        }
        const session = await mongoose.startSession();
        try {

            await session.startTransaction();

            const newTeacherDocuments = await TeacherDocument.create({
                empid,
                aadharCard,bankName,bankNo,pandCardNo,teacherPhoto,experienceLetter,idProof,otherDocuments1,otherDocuments2,
                salary:{
                    amount:salary_amount,
                    pfNo,pfAmount,tdsAmount,professionalTaxAmount,daAmount,hraAmount,salaryRemark
                },
                leave:{
                    cascualLeave,payEarnLeave,sickLeave,otherLeave
                }
            })

            const newUser = await User({
                name:firstname+" "+lastName,
                email,
                loginText:empid,
                userType:3,
                schoolId,
                yearId,
            })
            newUser.setPassword(empid)
            await newUser.save();


            const newTeacher = await Teacher.create({
                empid,
                firstname,
                lastName,
                middleName,
                email,
                gender,
                phoneNumber,
                address,
                qualification,
                dob,
                bloodGroup,
                dateOfJoining,
                documentId:newTeacherDocuments._id,
                userId:newUser._id,
                schoolId,
                yearId,
            })
            return {
                data:{
                    newTeacher,
                    newUser,
                    message:"New Teacher Added",
                    total:await Teacher.find({schoolId,yearId}).countDocuments()
                }

            }

        }catch(error){
            console.log(error)
            await session.abortTransaction();
            session.endSession();
            TE("Error Occur: "+error.message)
        }
    }


    this.getAllTeacher = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {skip,limit,teacherName} = body
        const searchParams = {}

        searchParams['schoolId'] = schoolId
        searchParams['yearId'] = yearId

        if(teacherName){
            searchParams['firstname'] = new RegExp(teacherName,'i')
        }

        const teacher = await Teacher.find(searchParams).skip(skip).limit(limit)

        return {
            data:{
                teacher,
                totalCount: teacher.length,
                total:await Teacher.find({schoolId,yearId}).countDocuments(),
                paginate:{
                    skip,limit
                },
                filter:{
                    searchParams
                }
            }
        }
    }

    this.getTeacher = async({params,decoded}) => {
        const {teacherId} = params
        const {schoolId,yearId} = decoded

        const teacher = await Teacher.findById(teacherId).populate('documentId').populate('userId')
        return {
            data:{
                teacher
            }
        }

    }

    this.searchTeacher = async({decoded,query}) => {
        const {teacherId,yearId} = decoded
        const {q} = query
        const teacher = await Teacher.find({teacherId,yearId}).or([
            {firstName:new RegExp(q,'i')},
            {middleName:new RegExp(q,'i')},
            {lastName:new RegExp(q,'i')},
            {empid:new RegExp(q,'i')},
            {middleName:new RegExp(q,'i')},
        ])
        return {
            data:{
                teacher,
                total:teacher.length,
                filter:{
                    q
                }
            }
        }


    }
    return this;
  })();