const Timetable = require('../../models/classTimeTable')
const {
    getTimeTableTotal,
    saveTimeTable,
    getTimeTableData,
    deleteTimeTableFromDB,
    getIndividualTimetable,
    getTeacherTimeTableTotal,
    saveTeacherTimeTable,
    getTeacherTimeTableData
} = require("../../helper/timetable/timetableHelper")
module.exports = (function () {

    this.createTimetable = async ({body,decoded}) => {
        const {name,timetableStructure,classId} = body
        const {schoolId,yearId} = decoded

        if(!name || !timetableStructure || !classId) throw new Error("Invalid Parameter: name, timetableStructure,.classId is required")

        if(await getTimeTableTotal({schoolId,yearId,name}) != 0){
            TE("Timetable name is already exists in system")
        }

        const data = {
            schoolId,yearId,name,timetableStructure,classId
        }
        return {
            data:{
                newTimetable: await saveTimeTable(data),
                total:await getTimeTableTotal({schoolId,yearId})
            }
        }
    }
    this.updateTimetable = async({body,decoded}) => {
        const {name,timetableId,timetableStructure,status,classId} = body
        const {schoolId,yearId} = decoded
        if(!timetableId) throw new Error("Cannot Find Timetable")        

        const data = {}
        if(name) data.name = name
        if(timetableStructure) data.timetableStructure = timetableStructure
        if(status) data.status = status
        if(classId) data.classId = classId

        return {
            data:{
                updatedTimeTable:await saveTimeTable(data,timetableId),
                total:await getTimeTableTotal({schoolId, yearId})
            }
        }

    }
    this.getAllTimetable = async ({body,decoded}) => {
        const {skip,limit,classId,status} = body
        const {schoolId,yearId} = decoded

        let query = {}
        query.yearId = yearId
        query.schoolId = schoolId
        if(classId) query.classId = classId
        if(status) query.status = status

        const all_data = await getTimeTableData(query,skip,limit)
        const totalPageCount = all_data.length
        return {
            data:{
                all_data,
                totalPageCount,
                total:await getTimeTableTotal({schoolId,yearId}),
                pagination:{
                    skip:skip,limit:limit,
                },
                filter:{
                    classId,status
                }
            }
        }
    }
    this.deleteTimetable = async ({body,decoded,params}) => {
        const {schoolId,yearId} = decoded
        const {timetableId} = params

        if(!await getTimeTableTotal({_id:timetableId})) throw new Error("Cannot Find Time Table")

        return {
            data:{
                deletedTimeTable:await deleteTimeTableFromDB(timetableId),
                total:await getTimeTableTotal({schoolId, yearId})
            }
        }


    }
    this.getIndividualTimetable = async({body,decoded,params}) => {
        const { timetableId } = params
        if(!timetableId || !verifyObjectId(timetableId)){
            TE("Timetable ID is required")
        }

        return {
            data:{
                timetable:await getIndividualTimetable( timetableId )
            }
        }
    }
    this.createTeacherTimetable = async ({body,decoded}) => {
        const {name,timetableStructure,teacherId} = body
        const {schoolId,yearId} = decoded

        if(!name || !timetableStructure || !teacherId) throw new Error("Invalid Parameter: name, timetableStructure,teacherId is required")

        if(await getTeacherTimeTableTotal({schoolId,yearId,name}) != 0){
            TE("Teacher Timetable name is already exists in system")
        }

        const data = {
            schoolId,yearId,name,timetableStructure,teacherId
        }

        return {
            data:{
                newTimetable: await saveTeacherTimeTable(data),
                total:await getTeacherTimeTableTotal({schoolId,yearId})
            }
        }
    }
    this.updateTeacherTimetable = async ({body,decoded}) => {
        const {name,timetableId,timetableStructure,status,teacherId} = body
        const {schoolId,yearId} = decoded
        if(!timetableId) throw new Error("Cannot Find Teacher Timetable")        

        const data = {}
        if(name) data.name = name
        if(timetableStructure) data.timetableStructure = timetableStructure
        if(status) data.status = status
        if(teacherId) data.teacherId = teacherId

        return {
            data:{
                updatedTimeTable:await saveTeacherTimeTable(data,timetableId),
                total:await getTeacherTimeTableTotal({schoolId, yearId})
            }
        }
    }
    this.getAllTeacherTimetable = async ({body,decoded}) => {
        const {skip,limit,teacherId,status} = body
        const {schoolId,yearId} = decoded
        let query = {}
        query.yearId = yearId
        query.schoolId = schoolId
        if(teacherId) query.teacherId = teacherId
        if(status) query.status = status
        const all_data = await getTeacherTimeTableData(query,skip,limit)
        const totalPageCount = all_data.length
        return {
            data:{
                all_data,
                totalPageCount,
                total:await getTeacherTimeTableTotal({schoolId,yearId}),
                pagination:{
                    skip:skip,limit:limit,
                },
                filter:{
                    teacherId:teacherId !== undefined? teacherId : null,
                    status:status != undefined ? status : null,
                }
            }
        }
    }
    return this;
  })();