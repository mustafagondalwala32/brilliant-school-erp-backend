const School = require('../../models/school')
const StudentDocument = require('../../models/studentDocument')
const Student = require('../../models/student')
const User = require('../../models/user')
const Class = require('../../models/class')
const RegisterStudent = require('../../models/registerStudent')
const mongoose = require('mongoose')


module.exports = (function () {
    /**
     * Create new School
     * @param {*} param0 
     */
    this.createNewStudent = async({body,decoded}) => {
       const {schoolId,yearId} = decoded
       const {classId,rollNo,studentName,fatherName,motherName,lastName,handicapped,parentContactNo1,parentContactNo2,parentEmail,age,dob,gender,address,block,district,state,pincode,religion,caste,student_photo,fatherOccupation,motherOccupation,guardianOccupation,fatherQualifiedName,motherQualifiedName,mother_photo,father_photo,studentAadharCardNo,fatherAadharCardNo,fatherBankName,fatherBankNameNo,studentBankName,studentBankNo,last_marksheet,transferCertificate,incomeCertificate,castCertificate,dobCertificate,studentAadharCard,fatherAadharCard,parentName,parentLoginText,parentLoginPassword,guardianName} = body

       if(!classId || !rollNo || !studentName || !lastName || !parentContactNo1 || !dob || !gender || !address || !age || !dob || !gender || !religion || !caste || !student_photo){
            TE("Invalid Parameter: classId,registerNo,studentName,lastName,parentContactNo1,dob,gender,address,age,dob,gender,religion,caste,student_photo is required")
        }

        const session = await mongoose.startSession();
        try {

            await session.startTransaction();

            const all_class = await Class.findById(classId)
            if(all_class == null){
                TE("Cannot find class please contact the admin")
            }
            all_class.currentRollNo += 1;

            const newStudentDocument = await StudentDocument.create({
                fatherOccupation,motherOccupation,guardianOccupation,fatherQualifiedName,motherQualifiedName,student_photo,father_photo,mother_photo,studentAadharCardNo,fatherAadharCardNo,fatherBankName,fatherBankNameNo,studentBankName,studentBankNo,last_marksheet,transferCertificate,incomeCertificate,castCertificate,dobCertificate,studentAadharCard,fatherAadharCard
            })

            const newStudentUser = await User({
                name:studentName,
                loginText:rollNo,
                userType:5,
                schoolId,
                yearId,
            })
            newStudentUser.setPassword(rollNo)
            await newStudentUser.save();

            const newStudent = await Student.create({
                schoolId,yearId,classId,rollNo,studentName,fatherName,motherName,lastName,guardianName,handicapped,parentContactNo1,parentContactNo2,parentEmail,dob,age,gender,address,block,district,state,pincode,religion,caste,student_photo,documentId:newStudentDocument._id,userId:newStudentUser._id
            })

            await all_class.save();


            const newParentUser = await User({
                name:parentName,
                loginText:parentLoginText,
                userType:4,
                schoolId,
                yearId
            })
            newParentUser.setPassword(parentLoginPassword)
            await newParentUser.save();


            await session.commitTransaction();
            session.endSession();

            return {
                data:{
                 newStudent,
                 newStudentUser,
                 newParentUser,
                 message:"New Student Added",
                 total:await Student.find({schoolId,yearId}).countDocuments()
                }
            }

        }catch(error){
            console.log(error)
            await session.abortTransaction();
            session.endSession();
            TE("Error Occur: "+error.message)
        }
    }
    this.fetchAllStudent = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {skip,limit} = body
        const {studentName,lastName,state,pincode,district,gender,classId} = body
        const searchParams = {}
        searchParams['schoolId'] = schoolId
        searchParams['yearId'] = yearId

        if(studentName){
            searchParams['studentName'] = new RegExp(studentName,'i')
        }
        if(lastName){
            searchParams['lastName'] = new RegExp(lastName,'i')
        }
        if(state){
            searchParams['state'] = state
        }
        if(pincode){
            searchParams['pincode'] = new RegExp(pincode,'i')
        }
        if(district){
            searchParams['district'] = new RegExp(district,'i')
        }
        if(gender){
            searchParams['gender'] = gender
        }
        if(classId){
            searchParams['classId'] = classId
        }
        const students = await Student.find(searchParams).skip(skip).limit(limit)

        return {
            data:{
                students,
                totalCount:students.length,
                total:await Student.find({schoolId,yearId}).countDocuments(),
                pagination:{
                    skip:skip,limit:limit
                },
                filters:searchParams
            }
        }

    }
    this.updateStudent = async({body,decoded}) => {
       const {schoolId,yearId} = decoded
       const {studentId,classId,rollNo,studentName,fatherName,motherName,lastName,handicapped,parentContactNo1,parentContactNo2,parentEmail,age,dob,gender,address,block,district,state,pincode,religion,caste,student_photo,fatherOccupation,motherOccupation,guardianOccupation,fatherQualifiedName,motherQualifiedName,mother_photo,father_photo,studentAadharCardNo,fatherAadharCardNo,fatherBankName,fatherBankNameNo,studentBankName,studentBankNo,last_marksheet,transferCertificate,incomeCertificate,castCertificate,dobCertificate,studentAadharCard,fatherAadharCard,parentName,parentLoginText,parentLoginPassword,guardianName} = body
    //    const {registerStudentId,classId,registerNo,studentName,fatherName,motherName,lastName,
        // parentContactNo1,parentContactNo2,dob,gender,address,block,district,state,pincode,student_photo,mother_photo,father_photo} = body
       
       
        if(!studentId){
           TE("Student Id is required")
       }
       const student = await Student.findById(studentId)
        if(!student){
            TE("Cannot find Student")
        }
        if(studentName){
            student.studentName = studentName
        }
        if(fatherName){
           student.fatherName = fatherName
        }
        if(motherName){
            student.motherName = motherName
        }
        if(lastName){
            student.lastName = lastName
        }
        if(guardianName){
            student.guardianName = guardianName
        }
        if(handicapped){
            student.handicapped = handicapped
        }
        if(parentContactNo1){
            student.parentContactNo1 = parentContactNo1
        }
        if(parentContactNo2){
            student.parentContactNo2 = parentContactNo2
        }
        if(parentEmail){
            student.parentEmail = parentEmail
        }
        if(dob){
            student.dob = dob
        }
        if(gender){
            student.gender = gender
        }
        if(address){
            student.address = address
        }
        if(block){
            student.block = block
        }
        if(district){
            student.district = district
        }
        if(state){
            student.state = state
        }
        if(pincode){
            student.pincode = pincode
        }
        if(religion){
            student.religion = religion
        }
        if(caste){
            student.caste = caste
        }
        if(student_photo){
            student.student_photo = student_photo
        }
        await student.save();
        const studentDocument = await StudentDocument.findById(student.documentId)

        if(fatherOccupation){
            studentDocument.fatherOccupation = fatherOccupation
        }
        if(motherOccupation){
            studentDocument.motherOccupation = motherOccupation
        }
        if(guardianOccupation){
            studentDocument.guardianOccupation = guardianOccupation
        }
        if(fatherQualifiedName){
            studentDocument.fatherQualifiedName = fatherQualifiedName
        }
        if(motherQualifiedName){
            studentDocument.motherQualifiedName = motherQualifiedName
        }
        if(student_photo){
            studentDocument.student_photo = student_photo
        }
        if(father_photo){
            studentDocument.father_photo = father_photo
        }
        if(mother_photo){
            studentDocument.mother_photo = mother_photo
        }
        if(studentAadharCardNo){
            studentDocument.studentAadharCardNo = studentAadharCardNo
        }
        if(fatherAadharCardNo){
            studentDocument.fatherAadharCardNo = fatherAadharCardNo
        }
        if(fatherBankName){
            studentDocument.fatherBankName = fatherBankName
        }
        if(fatherBankNameNo){
            studentDocument.fatherBankNameNo = fatherBankNameNo
        }
        if(studentBankName){
            studentDocument.studentBankName = studentBankName
        }
        if(studentBankNo){
            studentDocument.studentBankNo = studentBankNo
        }
        if(last_marksheet){
            studentDocument.last_marksheet = last_marksheet
        }
        if(transferCertificate){
            studentDocument.transferCertificate = transferCertificate
        }
        if(incomeCertificate){
            studentDocument.incomeCertificate = incomeCertificate
        }
        if(castCertificate){
            studentDocument.castCertificate = castCertificate
        }
        if(dobCertificate){
            studentDocument.dobCertificate = dobCertificate
        }
        if(studentAadharCard){
            studentDocument.studentAadharCard = studentAadharCard
        }
        if(fatherAadharCard){
            studentDocument.fatherAadharCard = fatherAadharCard
        }

        await studentDocument.save();
        const updateStudent = await Student.findById(studentId).populate('documentId')
        return {
            data:{
                message:"Register Student Updated",
                updateStudent
            }
        }



    }
    this.addUpdateStudentRollNo = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {classId,rollNoString,rollNo} = body
        
        const allclass = await Class.findById(classId)
        if(allclass == null){
            TE("Cannot Find Class. Please contact the administrator")
        }
        const school = await School.findById(schoolId)
        if(school == null){
            TE("Cannot Find School. Please contact the administrator")
        }

        const {uniqueCode} = school

        allclass.rollNoString = uniqueCode+"-"+rollNoString+"-"
        allclass.currentRollNo = rollNo;

        await allclass.save()

        return {
            data:{
                allclass,
                message:"Roll No Updated"
            }
        }
    }

    this.getSearchStudent = async({body,params,decoded}) => {
        const {schoolId,yearId} = body
        const {q} = params
        const students = await Student.find().or([
            {rollNo:new RegExp(q,'i')},
            {studentName:new RegExp(q,'i')},
            {fatherName:new RegExp(q,'i')},
        ])

        return {
            data:{
                students,
                total:students.length,
                filter:{
                    q
                }
            }
        }
    }
    return this;
  })();