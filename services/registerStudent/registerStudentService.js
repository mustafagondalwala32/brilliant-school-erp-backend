const School = require('../../models/school')
const RegisterStudent = require('../../models/registerStudent')
module.exports = (function () {

    /**
     * Create new School
     * @param {*} param0 
     */
    this.createNewRegisterStudent = async({body,decoded}) => {
       const {schoolId,yearId} = decoded
       const {classId,registerNo,studentName,fatherName,motherName,lastName,parentContactNo1,parentContactNo2,dob,gender,address,block,district,state,pincode,student_photo,mother_photo,father_photo} = body
       if(!classId || !registerNo || !studentName || !lastName || !parentContactNo1 || !dob || !gender || !address ){
           TE("Invalid Parameter: classId,registerNo,studentName,lastName,parentContactNo1,dob,gender,address is required")
       }
       const newRegisterStudent = await RegisterStudent.create({
           classId,registerNo,studentName,fatherName,motherName,lastName,parentContactNo1,parentContactNo2,dob,gender,address,block,district,state,pincode,student_photo,mother_photo,father_photo,schoolId,yearId
       })

       return {
           data:{
            newRegisterStudent,
            message:"New Register Student Added",
            total:await RegisterStudent.find({schoolId,yearId}).countDocuments()
           }
       }
    }


    this.fetchAllRegisterStudent = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {skip,limit} = body
        const {studentName,lastName,state,pincode,district,gender,classId} = body
        const searchParams = {}
        searchParams['schoolId'] = schoolId
        searchParams['yearId'] = yearId

        if(studentName){
            searchParams['studentName'] = new RegExp(studentName,'i')
        }
        if(lastName){
            searchParams['lastName'] = new RegExp(lastName,'i')
        }
        if(state){
            searchParams['state'] = state
        }
        if(pincode){
            searchParams['pincode'] = new RegExp(pincode,'i')
        }
        if(district){
            searchParams['district'] = new RegExp(district,'i')
        }
        if(gender){
            searchParams['gender'] = gender
        }
        if(classId){
            searchParams['classId'] = classId
        }
        const registerStudent = await RegisterStudent.find(searchParams).skip(skip).limit(limit)
        return {
            data:{
                registerStudent,
                totalCount:registerStudent.length,
                total:await RegisterStudent.find({schoolId,yearId}).countDocuments(),
                pagination:{
                    skip:skip,limit:limit
                },
                filters:searchParams
            }
        }

    }

    this.updateRegisterStudent = async({body,decoded}) => {
       const {schoolId,yearId} = decoded
       const {registerStudentId,classId,registerNo,studentName,fatherName,motherName,lastName,
        parentContactNo1,parentContactNo2,dob,gender,address,block,district,state,pincode,student_photo,mother_photo,father_photo} = body
       if(!registerStudentId){
           TE("Register Student Id is required")
       }
       const registerStudent = await RegisterStudent.findById(registerStudentId)
       if(!registerStudent){
        TE("Cannot find Register Student")
       }
       if(classId){
            registerStudent.classId = classId
       }
       if(registerNo){
            registerStudent.registerNo = registerNo
        }
        if(studentName){
            registerStudent.studentName = studentName
       }
       if(fatherName){
            registerStudent.fatherName = fatherName
        }
        if(motherName){
            registerStudent.motherName = motherName
        }
        if(lastName){
            registerStudent.lastName = lastName
        }
        if(parentContactNo1){
            registerStudent.parentContactNo1 = parentContactNo1
        }
        if(parentContactNo2){
            registerStudent.parentContactNo2 = parentContactNo2
        }
        if(dob){
            registerStudent.dob = dob
        }
        if(gender){
            registerStudent.gender = gender
        }
        if(address){
            registerStudent.address = address
        }
        if(block){
            registerStudent.block = block
        }
        if(district){
            registerStudent.district = district
        }
        if(state){
            registerStudent.state = state
        }
        if(pincode){
            registerStudent.pincode = pincode
        }
        if(student_photo){
            registerStudent.student_photo = student_photo
        }
        if(mother_photo){
            registerStudent.mother_photo = mother_photo
        }
        if(father_photo){
            registerStudent.father_photo = father_photo
        }
        await registerStudent.save();
        return {
            data:{
                message:"Register Student Updated"
            }
        }



    }
    return this;
  })();