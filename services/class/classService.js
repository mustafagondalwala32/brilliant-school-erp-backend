const School = require('../../models/school')
const Class = require('../../models/class')
const Teacher = require('../../models/teacher')
const {
    getTotalClass,
    saveClass
} = require("../../helper/class/classHelper")

module.exports = (function () {

    /**
     * Create new School
     * @param {*} param0 
     */
    this.createNewClass = async({body,decoded}) => {
        const {name} = body
        const {schoolId,yearId} = decoded

        const isExist = await Class.findOne({schoolId,yearId,className:name})
        if(isExist != null){
            TE("Class Name is already Existing")
        }
        const newClass = await Class.create({
            schoolId,yearId,className:name
        });

       return {
           data:{
            newClass,
            message:"New Class is being Created",
            total:await Class.find({schoolId,yearId}).countDocuments()
           }
       }
    }


    this.deleteClass = async({body,decoded,params}) => {
        const {schoolId,yearId} = decoded
        const {classId} = params


        const deleteClass = await Class.findById(classId)
        if(deleteClass == null){
            TE("Cannot Found Class")
        }
        if(deleteClass.delete() == null){
            TE("Error Occured while Deleting Class. Please try again later")
        }

        return {
            data:{
                message:"Class Deleted",
                total:await Class.find({schoolId,yearId}).countDocuments()
            }
        }
    }


    this.fetchAllClass = async ({body,decoded,params}) => {
        const {schoolId,yearId} = decoded
        const {skip,limit} = body

        const classes = await Class.find({schoolId,yearId}).sort({updatedAt:-1}).skip(skip).limit(limit)
        return {
            data:{
                classes,
                totalCount:classes.length,
                total:await Class.find({schoolId,yearId}).countDocuments(),
                pagination:{
                   skip,limit
                }
            }
        }
    }

    this.updateClass = async ({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {name,section,classId} = body

        const updateClass = await Class.findById(classId)

        if(updateClass == null){
            TE("Cannot Find Class")
        }
        if(updateClass.schoolId != schoolId || updateClass.yearId != yearId){
            TE("Invalid Class. Please contact administrator")
        }

        if(name){
            updateClass.className = name
        }   
        if(section){
            updateClass.section = section
        }
        await updateClass.save()

        return {
            data:{
                updateClass,
                total:await Class.find({schoolId,yearId}).countDocuments(),
            }
        }
    }

    this.createNewSection = async ({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {name,section} = body

        const singleClassInstance = await Class.findOne({schoolId,yearId,className:name,section })
        if(singleClassInstance == null && singleClassInstance.section == "") { 
            singleClassInstance.section = section
            return {
                newSection:await singleClassInstance.save(),
                message:"New Class Section Added",
                total:await Class.find({schoolId,yearId}).countDocuments(),
            }
        }else{
            const newClass = await Class.create({
                schoolId,yearId,className:name,section
            })
            return {
                newSection:newClass,
                message:"New Class Section Added",
                total:await Class.find({schoolId,yearId}).countDocuments(),
            }
        }
    }

    this.assignTeacherToClass = async ({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {teacherId,classId} = body
        
        // check if class is existed
        const classInstance = await Class.findById(classId)

        if(classInstance == null){
            TE("Cannot Found the class")
        }

        const teacherInstance = await Teacher.findById(teacherId)
        if(teacherInstance == null){
            TE("Cannot Find the teacher")
        }

        teacherInstance.assignedClassId = classId
        classInstance.assignTeacherId = teacherId

        await teacherInstance.save()
        await classInstance.save()

        return {
            data:{
                message:"Teacher is assigned to Class"
            }
        }
    }
    this.assignSubjectToClass = async({body,decoded}) => {
        const {classId,subjects} = body
        const {schoolId,yearId} = decoded

        if(!classId || !subjects) TE("Class Id and Subjects is required")
        let classExist = {
            _id: classId
        }
        if(await getTotalClass(classExist) != 1){
            TE("Cannot Found Class")
        }
        const data = {}
        data.subjects = subjects

        return {
            data:{
                updatedClass:await saveClass(data,classId),
                total:await getTotalClass({schoolId,yearId})
            }
        }
    }
    return this;
  })();