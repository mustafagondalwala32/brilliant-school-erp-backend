const Subject = require('../../models/subject')




module.exports = (function () {

    this.createSubject = async ({body,decoded}) => {
        const {name} = body
        const {schoolId,yearId} = decoded

        const isExist = await Subject.find({schoolId,yearId,subjectName:name}).countDocuments()

        if(isExist != 0){
            TE("Subject is already exists in system")
        }

        const newSubject = await Subject.create({
            schoolId,yearId,subjectName:name
        })


        return {
            data:{
                newSubject,
                total:await Subject.find({schoolId,yearId}).countDocuments()
            }
        }
    }
    this.updateSubject = async({body,decoded}) => {
        const {name,subjectId,status} = body
        const {schoolId,yearId} = decoded
        const subject = await Subject.findById(subjectId)
        if(subject == null){
            TE("Cannot find subject in school")
        }
        if(name){
            subject.subjectName = name
        }
        if(status){
            subject.status = status
        }
        await subject.save()
        return {
            data:{
                updatedSubject:subject,
                total:await Subject.find({schoolId,yearId}).countDocuments()
            }
        }
    }
    this.getAllSubject = async ({body,decoded}) => {
        const {skip,limit,search} = body
        const {schoolId,yearId} = decoded
        
        const searchParams = {}
        searchParams['schoolId'] = schoolId
        searchParams['yearId'] = yearId

        if(search){
            searchParams['subjectName'] = new RegExp(search,'i')
        }

        const all_subject = await Subject.find(searchParams).sort('-updatedAt').skip(skip).limit(limit)
        return {
            data:{
                all_subject: all_subject,
                totalPage:all_subject.length,
                total:await Subject.find({schoolId,yearId}).countDocuments(),
                pagination:{
                    skip:skip,limit:limit,
                },
                filter:{
                    search
                }
            }
        }

    }
    this.deleteSubject = async ({body,decoded,params}) => {
        const {schoolId,yearId} = decoded
        const {subjectId} = params
        const subject = await Subject.findById(subjectId);
        if(subject == null){
            TE("Cannot Find Subject")
        }
        if(subject.schoolId != schoolId || subject.yearId != yearId){
            TE("Un Authorization Request")
        }
        await subject.delete();
        return {
            data:{
                message:"Subject Removed Successfully",
                total:await Subject.find({schoolId,yearId}).countDocuments()
            }
        }
    }
    return this;
  })();