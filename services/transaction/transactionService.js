const {
    getTotalTransaction,
    saveTransaction,
    deleteTransactionFromDB,
    getTransactionData
} = require("../../helper/transaction/transactionHelper")


module.exports = (function () {
    this.createTransaction = async ({body,decoded}) => {
        // get schoolId and yearId
        const {schoolId,yearId} = decoded

        // get all the data from body
        const {partyType,type,date,name,fatherName,address,contact_no,totalAmount,billNo,billDate,billImage,paymentMode,remark} = body
        if(!type || !date || !name  || !contact_no || !remark || !totalAmount || !partyType){
            // validate the data
            TE("Invalid Parameter: type,date,name,contact_no,remark,totalAmount,partyType  is required")
        }

        const typeArray = [1,2]
        const partyTypeArray = [1,2,3]
        const paymentModeArray = [1,2,3]
        // more validation
        if(!typeArray.includes(type)) TE("Invalid Parameter: type should be either 1 or 2")
        if(!partyTypeArray.includes(partyType)) TE("Invalid Parameter: party type should be either 1 or 2 or 3")
        if(!paymentModeArray.includes(paymentMode)) TE("Invalid Parameter: party type should be either 1 or 2 or 3")


        // make the saving array
        const data = {
            schoolId,yearId,partyType,type,date,name,fatherName,address,contact_no,totalAmount,billNo,billDate,billImage,paymentMode,remark
        }

        // save data and send to response
        return {
            data:{
                newTransaction:await saveTransaction(data),
                total:await getTotalTransaction({schoolId,yearId})
            }
        }
    }
    this.updateTransaction = async({body,decoded}) => {
        // get all the parameters
        const {schoolId,yearId} = decoded
        const {transaction_id,partyType,type,date,name,address,contact_no,totalAmount,billNo,billDate,billImage,paymentMode,remark} = body


        // get the data from db
        let query = {
            _id:transaction_id
        }
        
        // check is already exist
        if(await getTotalTransaction(query) == 0){
            TE("Cannot Find Transaction")
        }

        const data = {}
        
        if(!partyType) data.partyType = partyType
        if(!type) data.type = type
        if(!date) data.date = date
        if(!name) data.name = name
        if(!contact_no) data.contact_no = contact_no
        if(!address) data.address = address
        if(!totalAmount) data.totalAmount = totalAmount
        if(!billNo) data.billNo = billNo
        if(!billDate) data.billDate = billDate
        if(!billImage) data.billImage = billImage
        if(!paymentMode) data.paymentMode = paymentMode
        if(!remark) data.remark = remark

       return {
            data:{
                updateTransaction:await saveTransaction(data,transaction_id),
                total:await getTotalTransaction({schoolId,yearId})
            }
        }
    }
    this.deleteTransaction = async ({body,decoded,params}) => {
        const {schoolId,yearId} = decoded
        const {transaction_id} = params
        if(await getTotalTransaction({_id:transaction_id})  == 0){
            TE("Cannot Find Transaction")
        }
        return {
            data:{
                deletedTransaction:await deleteTransactionFromDB(transaction_id),
                total:await getTotalTransaction({schoolId, yearId})
            }
        }
    }
    this.getAllTransaction = async ({body,decoded}) => {
        // get the schoolId and year Id 
        const {schoolId,yearId} = decoded

        // santilize the skip and limit
        const {skip,limit} = getSkipLimitFromBody(body)

        // define the filter values
        const filterValues = [
            "from","to","name","transaction_id","type","partyType","name","contact_no","billNo","paymentMode","remark"
        ]

        // santize the filter values
        const {from,to,transaction_id,type,partyType,name,contact_no,billNo,paymentMode,remark} = getFilterValues(filterValues,body)

        // make query
        let query = {}
        query.yearId = yearId
        query.schoolId = schoolId

        if( transaction_id != null ) query._id = transaction_id
        if( type != null ) query.type = type
        if( partyType != null ) query.partyType = partyType
        if( paymentMode != null ) query.paymentMode = paymentMode


        if( name != null ) query.name = new RegExp(name,'i')
        if( contact_no != null ) query.contact_no = new RegExp(contact_no,'i')
        if( billNo != null ) query.billNo = new RegExp(billNo,'i')
        if( remark != null ) query.remark = new RegExp(remark,'i')



        if(from != null && to != null ) {
            query.date = {
                $gte: from, 
                $lt: to
            }
        }

        // get the data
        const all_data = await getTransactionData(query,skip,limit)

        return {
            data:{
                all_data,
                totalPage:all_data.length,
                total:await getTotalTransaction(query),
                totalCount:await getTotalTransaction({schoolId,yearId}),
                pagination:{
                    skip,
                    limit
                },
                filter:{
                    from,to,transaction_id,type,partyType,name,contact_no,billNo,paymentMode,remark
                }
            }
        }
    }
    return this;
  })();