const {
    savePeriod,
    getTotal,
    getData,
    deletePeriodFromDB
} = require("../../helper/period/periodHelper")

module.exports = (function () {
    /**
     * Create new School
     * @param {*} param0 
     */
    this.createNewPeriod = async({body,decoded}) => {
       const {schoolId,yearId} = decoded
       const {name,startTime,endTime} = body
    
       const data = {
           name,
           startTime,
           endTime,
           schoolId,
           yearId,
       }

       return {
           data:{
               period:await savePeriod(data),
               total:await getTotal({schoolId, yearId})
           }
       }
    }


    this.updatePeriod = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
       const {name,startTime,endTime,periodId} = body

        let query = {
            _id:periodId
        }
        if(await getTotal(query) == 0){
            TE("Cannot Find Period")
        }

        const data = {}

        if(name){
            data.name = name
        }
        
        if(startTime){
            data.startTime = startTime
        }
        
        if(endTime){
            data.endTime = endTime
        }

       return {
            data:{
                updatePeriod:await savePeriod(data,periodId),
                total:await getTotal({schoolId,yearId})
            }
        }
    }

    this.getAllPeriods = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {skip,limit} = body
        let query = {
            schoolId,yearId
        }
        const all_data = await getData(query,skip,limit)

        return {
            data:{
                all_data,
                totalPage:all_data.length,
                total:await getTotal(query)
            }
        }

    }
    this.deletePeriod = async ({decoded,params}) =>{
        const {schoolId,yearId} = decoded
        const {periodId} = params
        if(await getTotal({_id:periodId})  == 0){
            TE("Cannot Find Period")
        }
        return {
            data:{
                deletedPeriod:await deletePeriodFromDB(periodId),
                total:await getTotal({schoolId, yearId})
            }
        }
    }
    return this;
  })();