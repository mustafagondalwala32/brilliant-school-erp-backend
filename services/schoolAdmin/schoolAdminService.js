const School = require('../../models/school')
const User = require('../../models/user')


module.exports = (function () {

    /**
     * Create new School
     * @param {*} param0 
     */
    this.createSchoolAdmin = async({body}) => {
        const {email,password,schoolId} = body
        const school = await School.findById(schoolId)
        if(school == null){
            TE("Cannot Find School")
        }
        if(school.adminId != null){
            TE("Admin Already exists in school");
        }
        const user = new User({
            email,
            loginText:email,
            schoolId,
            userType:1,
            yearId:1
        })
        user.setPassword(password)
        const newUser = await user.save();

        school.adminId = newUser._id
        await school.save();

        return {
            data:{
                newUser,
                message:"New School Admin Created"
            }
        }
    }

    this.updateSchoolAdmin = async({body}) => {
        const {userId,email,password} = body
        const user = await User.findById(userId)
        if(user == null){
            TE("Cannot Find School Admin")
        }

        if(email){
            user.email = email
        }
        if(password){
            user.hash = bcrypt.hashSync(password, 10)
        }

        const updateUser = await user.save()
        return {
            data:{
                updateUser,
                message:"Updated School Admin User"
            }
        }
    }
    return this;
  })();