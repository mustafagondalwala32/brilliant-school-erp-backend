const {
    getTotalStaff,
    saveStaff,
    deleteStaffFromDB,
    getStaffData
} = require("../../helper/staff/staffHelper")


module.exports = (function () {
    this.createStaff = async ({body,decoded}) => {
        const { schoolId } = decoded
        const {employee_id,employee_name,gender,dob,relativeName,email,contactNo,address,qualification,bloodgroup,document_files,date_of_joining,designation,salary,leave} = body
        if(!employee_id || !employee_name || !gender || !dob || !relativeName || !contactNo || !date_of_joining || !designation || !salary) TE("Invalid Parameter: employee_id,employee_name,gender,dob,relativeName,contactNo,date_of_joining,designation,salary is required")
        
        const totalCountQuery = {
            schoolId,
            employee_id
        }
        if(getTotalStaff(totalCountQuery) > 0){
            TE("Employee id is already in system")
        }

        const data = {
            schoolId,employee_id,employee_name,gender,dob,relativeName,email,contactNo,address,qualification,bloodgroup,document_files,date_of_joining,designation,salary,leave
        }
        return {
            data:{
                newStaff: await saveStaff(data),
                total:await getTotalStaff(totalCountQuery)
            }
        }
    }
    this.updateStaff = async({body,decoded}) => {
        const {schoolId} = decoded
        const {employee_db_id,employee_id,employee_name,gender,dob,relativeName,email,contactNo,address,qualification,bloodgroup,document_files,date_of_joining,designation,salary,leave} = body
        if(!employee_id || !employee_name || !gender || !dob || !relativeName || !contactNo || !date_of_joining || !designation || !salary) TE("Invalid Parameter: employee_id,employee_name,gender,dob,relativeName,contactNo,date_of_joining,designation,salary is required")
        
        const totalCountQuery = {
            schoolId,
            _id:employee_db_id
        }
        if(getTotalStaff(totalCountQuery) == 0){
            TE("Cannot Find Staff in system")
        }
        
        const data = {
            schoolId,employee_id,employee_name,gender,dob,relativeName,email,contactNo,address,qualification,bloodgroup,document_files,date_of_joining,designation,salary,leave
        }
       return {
            data:{
                updateStaff:await saveStaff(data,employee_db_id),
                total:await getTotalStaff({schoolId})
            }
        }
    }
    this.deleteStaff = async ({body,decoded,params}) => {
        const {schoolId,} = decoded
        const {staffId} = params

        const totalCountQuery = {
            schoolId,
            _id:staffId
        }

        if(await getTotalStaff(totalCountQuery)  == 0){
            TE("Cannot Find Staff")
        }
        
        return {
            data:{
                deletedStaff:await deleteStaffFromDB(staffId),
                total:await getTotalStaff({schoolId})
            }
        }
    }
    this.getAllStaff = async ({body,decoded}) => {
        const {schoolId} = decoded
        const {employee_name,employee_id,employee_db_id} = body
        const {skip,limit} = getSkipLimitFromBody(body)

        let query = {}
        query.schoolId = schoolId

        if(employee_name != null && employee_name !== undefined) query.employee_name = new RegExp(employee_name,'i')
        if(employee_id != null && employee_id !== undefined) query.employee_id = new RegExp(employee_id,'i')
        if(employee_db_id != null && employee_db_id !== undefined) query._id = employee_db_id

        const all_data = await getStaffData(query,skip,limit)

        return {
            data:{
                all_data,
                totalPage:all_data.length,
                total:await getTotalStaff(query),
                pagination:{
                    skip,
                    limit
                },
                filter:{
                    employee_id:employee_id !== undefined ? employee_id : "",
                    employee_name: employee_name !== undefined ? employee_name : "",
                    employee_db_id: employee_db_id !== undefined ? employee_db_id : "",
                }
            }
        }
    }
    return this;
  })();