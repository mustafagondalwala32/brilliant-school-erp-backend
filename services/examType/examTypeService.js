const {
    getTotalExamType,
    saveExamType,
    deleteExamTypeFromDB,
    getExamTypeData
} = require("../../helper/examType/examTypeHelper")


module.exports = (function () {
    this.createExamType = async ({body,decoded}) => {
        // get schoolId and yearId
        const {schoolId,yearId} = decoded

        // get all the data from body
        const {name} = body
        if(!name){
            // validate the data
            TE("Invalid Parameter: name is required")
        }
        if(await getTotalExamType({schoolId,yearId,name}) > 0){
            TE("Exam Type is already exists")
        }
        
        // make the saving array
        const data = {
            schoolId,yearId,name
        }

        // save data and send to response
        return {
            data:{
                newExamType:await saveExamType(data),
                total:await getTotalExamType({schoolId,yearId})
            }
        }
    }
    this.updateExamType = async({body,decoded}) => {
        // get all the parameters
        const {schoolId,yearId} = decoded
        const {examtype_id,name,status} = body


        // get the data from db
        let query = {
            _id:examtype_id
        }
        
        // check is already exist
        if(await getTotalExamType(query) == 0){
            TE("Cannot Find ExamType")
        }

        const data = {}
        
    
        if(!name) data.name = name
        if(status !== undefined) data.status = status

        return {
            data:{
                updateExamType:await saveExamType(data,examtype_id),
                total:await getTotalExamType({schoolId,yearId})
            }
        }
    }
    this.deleteExamType = async ({body,decoded,params}) => {
        const {schoolId,yearId} = decoded
        const {examtype_id} = params
        if(await getTotalExamType({_id:examtype_id})  == 0){
            TE("Cannot Find ExamType")
        }
        return {
            data:{
                deletedExamType:await deleteExamTypeFromDB(examtype_id),
                total:await getTotalExamType({schoolId, yearId})
            }
        }
    }
    this.getAllExamType = async ({body,decoded}) => {
        // get the schoolId and year Id 
        const {schoolId,yearId} = decoded

        // santilize the skip and limit
        const {skip,limit} = getSkipLimitFromBody(body)

        // define the filter values
        const filterValues = [
            "status"
        ]

        // santize the filter values
        const {status} = getFilterValues(filterValues,body)

        // make query
        let query = {}
        query.yearId = yearId
        query.schoolId = schoolId

        if( status != null ) query.status = status

        // get the data
        const all_data = await getExamTypeData(query,skip,limit)

        return {
            data:{
                all_data,
                totalPage:all_data.length,
                total:await getTotalExamType(query),
                totalCount:await getTotalExamType({schoolId,yearId}),
                pagination:{
                    skip,
                    limit
                },
                filter:{
                    status
                }
            }
        }
    }
    return this;
  })();