const User = require('../../models/user')
const School = require('../../models/school')
const jwt = require("jsonwebtoken");
module.exports = (function () {


    this.login = async ({body}) => {
        const {loginText,password,schoolId} = body
        
        const school = await School.findById(schoolId)
        if(school == null){
            TE("Cannot Find School")
        }
        const yearId = school.currentYearId;
        const user = await User.findOne({loginText,schoolId,yearId})
        
        if(user == null){
            TE("Cannot Find User")
        }
        if(!user.validPassword(password)){
            TE("Cannot Find User")
        }
        const token = jwt.sign({ _id: user._id,userType:user.userType,schoolId:user.schoolId }, process.env.LOGIN_ADMIN_SECRETE, {
            expiresIn: "30 days"
        });
        return {
            data:{
                message:"Login Successfull",
                token,
                user
            }
        }
    }
    return this;
  })();