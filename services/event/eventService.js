const {
    getTotalEvent,
    saveEvent,
    deleteEventFromDB,
    getEventData
} = require("../../helper/event/eventHelper")


module.exports = (function () {
    this.createEvent = async ({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {name,from,to,description,classIds} = body

        if(!name || !description || !from || !to || !classIds) TE('Invalid Parameter: name,description,from,to,classIds is required')

        if(await getTotalEvent({schoolId,yearId,name}) > 0){
            TE("Event is already exists in system")
        }

        const data = {
            schoolId,yearId,name,from,to,description,classIds
        }
        return {
            data:{
                newEvent:await saveEvent(data),
                total:await getTotalEvent({schoolId,yearId})
            }
        }
    }
    this.updateEvent = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {name,date,description,eventId,classIds} = body

        let query = {
            _id:eventId
        }
        if(await getTotalEvent(query) == 0){
            TE("Cannot Find Event")
        }

        const data = {}

        if(name){
            data.name = name
        }
        
        if(date){
            data.date = date
        }
        
        if(description){
            data.description = description
        }
        if(classIds){
            data.classIds = classIds
        }

       return {
            data:{
                updateEvent:await saveEvent(data,eventId),
                total:await getTotalEvent({schoolId,yearId})
            }
        }
    }
    this.deleteEvent = async ({body,decoded,params}) => {
        const {schoolId,yearId} = decoded
        const {eventId} = params
        if(await getTotalEvent({_id:eventId})  == 0){
            TE("Cannot Find Event")
        }
        return {
            data:{
                deletedEvent:await deleteEventFromDB(eventId),
                total:await getTotalEvent({schoolId, yearId})
            }
        }
    }
    this.getAllEvent = async ({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {name,from,to,classIds,eventId} = body
        const {skip,limit} = getSkipLimitFromBody(body)

        let query = {}
        query.yearId = yearId
        query.schoolId = schoolId

        if(name != null && name !== undefined) query.name = new RegExp(name,'i')
        if(eventId != null && eventId !== undefined) query._id = eventId
        if(classIds != null && classIds !== undefined) query.classIds = {$in:classIds}
        if(from != null && from !== undefined && to != null && to !== undefined) {
            query.from = { $gte:from}
            query.to = { $lt:to} 
        }


        const all_data = await getEventData(query,skip,limit)

        return {
            data:{
                all_data,
                totalPage:all_data.length,
                total:await getTotalEvent(query),
                pagination:{
                    skip,
                    limit
                },
                filter:{
                    from: from != null && from !== undefined ? from : null,
                    to: to != null && to !== undefined ? to : null,
                    name: name != null && name !== undefined ? name : null,
                    classIds: classIds != null && classIds !== undefined ? classIds : null,
                    eventId: eventId != null && eventId !== undefined ? eventId : null,
                }
            }
        }
    }
    return this;
  })();