const {
    getTotalBus,
    saveBus,
    deleteBusFromDB,
    getBusData,
    getTotalRoutes,
    saveBusRoute,
    deleteRoute,
    getBusRouteData,
    saveBusStop,
    deleteBusStop
} = require("../../helper/bus/busHelper")


module.exports = (function () {
    this.createBus = async ({body,decoded}) => {
        // get schoolId and yearId
        const { schoolId } = decoded

        // get all the data from body
        const {name,company,model_no,bus_no,owner_name,owner_contact,register_no,capacity,document_images} = body
        if(!name || !company || !model_no || !bus_no || !owner_name || !owner_contact || !register_no) TE("Invalid Parameter: name,company,model_no,owner_name,owner_contact,capacity is required")

        // make the saving array
        const data = {
            schoolId,name,company,model_no,bus_no,owner_name,owner_contact,register_no,capacity,document_images
        }

        // save data and send to response
        return {
            data:{
                newBus:await saveBus(data),
                total:await getTotalBus({schoolId})
            }
        }
    }
    this.updateBus = async({body,decoded}) => {
        // get all the parameters
        const {schoolId,yearId} = decoded
        const {bus_id,name,company,model_no,bus_no,owner_name,owner_contact,register_no,capacity,document_images} = body


        // get the data from db
        let query = {
            _id:bus_id
        }
        
        // check is already exist
        if(await getTotalBus(query) == 0){
            TE("Cannot Find Bus")
        }

        const data = {}
        
        if(!company) data.company = company
        if(!model_no) data.model_no = model_no
        if(!bus_no) data.bus_no = bus_no
        if(!name) data.name = name
        if(!owner_name) data.owner_name = owner_name
        if(!owner_contact) data.owner_contact = owner_contact
        if(!register_no) data.register_no = register_no
        if(!capacity) data.capacity = capacity
        if(!document_images) data.document_images = document_images

       return {
            data:{
                updateBus:await saveBus(data,bus_id),
                total:await getTotalBus({schoolId})
            }
        }
    }
    this.deleteBus = async ({decoded,params}) => {
        const {schoolId} = decoded
        const {bus_id} = params
        if(await getTotalBus({_id:bus_id})  == 0){
            TE("Cannot Find Bus")
        }
        return {
            data:{
                deletedBus:await deleteBusFromDB(bus_id),
                total:await getTotalBus({schoolId})
            }
        }
    }
    this.getAllBus = async ({body,decoded}) => {
        // get the schoolId and year Id 
        const {schoolId,yearId} = decoded

        // santilize the skip and limit
        const {skip,limit} = getSkipLimitFromBody(body)

        // define the filter values
        const filterValues = [
           "name","bus_id",
        ]

        // santize the filter values
        const {name,bus_id} = getFilterValues(filterValues,body)

        // make query
        let query = {}
        query.schoolId = schoolId

        if( bus_id != null ) query._id = bus_id
        if( name != null ) query.name = name

        // get the data
        const all_data = await getBusData(query,skip,limit)

        return {
            data:{
                all_data,
                totalPage:all_data.length,
                total:await getTotalBus(query),
                totalCount:await getTotalBus({schoolId,yearId}),
                pagination:{
                    skip,
                    limit
                },
                filter:{
                    bus_id,name,
                }
            }
        }
    }
    this.createBusRoute = async ({body,decoded}) => {
        const { schoolId, yearId } = decoded
        const { route_name } = body;
        if(!route_name) TE("Invalid Parameter: Route Name is required")
        if( getTotalRoutes( { schoolId , yearId , route_name } ) > 0 ) TE("Route is Already Exists")
        let data = {
            schoolId,
            yearId,
            route_name
        }
        return {
            data:{
                newRoute:await saveBusRoute(data),
                total:await getTotalRoutes( { schoolId, yearId } )
            }
        }
    }
    this.updateBusRoute = async ({body,decoded}) => {
        const { schoolId, yearId } = decoded
        const { route_id , assigned_bus , route_name} = body
        
        if(!route_id || !verifyObjectId(route_id)) TE("Invalid Bus Route ID")
        let data = {}
        if(assigned_bus) data.assigned_bus = assigned_bus
        if(route_name) data.route_name = route_name

        return {
            data:{
                newRoute:await saveBusRoute(data,route_id),
                total:await getTotalRoutes( { schoolId, yearId } )
            }
        }
    }
    this.deleteBusRoute  = async ({body,decoded,params}) => {
        const {busroute_id} = params
        const { schoolId, yearId } = decoded

        let findTotalQuery = {
            schoolId,
            yearId,
            _id:busroute_id
        }
        if( await getTotalRoutes(findTotalQuery) != 1 ) TE("Cannot Find the Route")

        return {
            data:{
                deletedBusRoute:await deleteRoute(busroute_id),
                total:await getTotalRoutes( { schoolId, yearId } )
            }
        }
    }
    this.getAllBusRoute = async ({body,decoded}) => {
        const { schoolId, yearId } = decoded
        const {skip,limit} = getSkipLimitFromBody(body)
        const filterValues = [
            "route_name", "route_id"
        ]

        let query = {}
        query.schoolId = schoolId
        query.yearId = yearId


        const { route_name , route_id} = getFilterValues(filterValues,body)

        if(route_name != null) query.route_name = new RegExp(route_name, "i")
        if(route_id != null) query._id = route_id

        const all_data = await getBusRouteData(query,skip,limit)
        return {
            data:{
                all_data,
                totalPage:all_data.length,
                totalCount:await getTotalRoutes( { schoolId, yearId}),
                pagination:{
                    skip,limit
                },
                filter:{
                    route_name , route_id
                }
            }
        }
    }
    this.createBusRouteStop = async ({body,decoded}) => {
        const {schoolId, yearId} = decoded
        const { route_id, stop_name, time } =body
        
        if( getTotalRoutes( { schoolId , yearId , _id:route_id } ) > 0 ) TE("Route is Already Exists")
        
        let data = {
            stop_name, time
        }

        const updatedRoutes = await saveBusStop(data,route_id)
        return {
            data:{
                updatedRoutes,
                totalStops:updatedRoutes.stops.length
            }
        }
    }
    this.deleteBusRouteStop  = async ({body,decoded,params}) => {
       const { route_id, stop_id } = params
       const { schoolId, yearId } = decoded

       if( getTotalRoutes( { schoolId , yearId , _id:route_id } ) <= 0 ) TE("Cannot Find Route")

       const updatedRoutes = await deleteBusStop(stop_id,route_id)
       return {
           data:{
               updatedRoutes,
               totalStops:updatedRoutes.stops.length
           }
       }
    }
    return this;
  })();