const {
    getTotalHoliday,
    saveHolidayPeriod,
    deleteHolidayFromDB,
    getHolidayData
} = require("../../helper/holiday/holidayHelper")


module.exports = (function () {
    this.createHoliday = async ({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {name,date,description} = body

        if(!name || !description || !date) TE('Invalid Parameter: name,description,date is required')

        if(await getTotalHoliday({schoolId,yearId,name}) > 0){
            TE("Holiday is already exists in system")
        }

        const data = {
            schoolId,yearId,name,date,description
        }
        return {
            data:{
                newHoliday:await saveHolidayPeriod(data),
                total:await getTotalHoliday({schoolId,yearId})
            }
        }
    }
    this.updateHoliday = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {name,date,description,holidayId} = body

        let query = {
            _id:holidayId
        }
        if(await getTotalHoliday(query) == 0){
            TE("Cannot Find TimeTable")
        }

        const data = {}

        if(name){
            data.name = name
        }
        
        if(date){
            data.date = date
        }
        
        if(description){
            data.description = description
        }

       return {
            data:{
                updateHoliday:await saveHolidayPeriod(data,holidayId),
                total:await getTotalHoliday({schoolId,yearId})
            }
        }
    }
    this.deleteHoliday = async ({body,decoded,params}) => {
        const {schoolId,yearId} = decoded
        const {holidayId} = params
        if(await getTotalHoliday({_id:holidayId})  == 0){
            TE("Cannot Find Holiday")
        }
        return {
            data:{
                deletedHoliday:await deleteHolidayFromDB(holidayId),
                total:await getTotalHoliday({schoolId, yearId})
            }
        }
    }
    this.getAllHoliday = async ({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {name,from,to,holidayId} = body
        const {skip,limit} = getSkipLimitFromBody(body)

        let query = {}
        query.yearId = yearId
        query.schoolId = schoolId

        if(name != null && name !== undefined) query.name = new RegExp(name,'i')
        if(holidayId != null && holidayId !== undefined) query._id = holidayId
        if(from != null && from !== undefined && to != null && to !== undefined) {
            query.updatedAt = {
                $gte: from, 
                $lt: to
            }
        }


        const all_data = await getHolidayData(query,skip,limit)

        return {
            data:{
                all_data,
                totalPage:all_data.length,
                total:await getTotalHoliday(query),
                pagination:{
                    skip,
                    limit
                },
                filter:{
                    from: from != null && from !== undefined ? from : null,
                    to: to != null && to !== undefined ? to : null,
                    name: name != null && name !== undefined ? name : null,
                    holidayId: holidayId != null && holidayId !== undefined ? holidayId : null,
                }
            }
        }
    }
    return this;
  })();