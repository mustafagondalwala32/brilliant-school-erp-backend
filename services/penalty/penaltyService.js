const {
    getTotalPenalty,
    savePenalty,
    deletePenaltyFromDB,
    getPenaltyData
} = require("../../helper/penalty/penaltyHelper")

const {
    getTotalStudent
} = require("../../helper/student/studentHelper")

module.exports = (function () {
    this.createPenalty = async ({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {student_id,amount,reason,remark} = body

        if(!student_id || !amount ) TE('Invalid Parameter: student_id,amount is required')

        if(!verifyObjectId(student_id)) TE('Invalid Parameter: Student Id is invalid')

        if(await getTotalStudent({_id:student_id}) != 1) TE("Invalid Parameter: Student is invalid")
        

        const data = {
            student_id,amount,reason,remark,schoolId,yearId
        }
        return {
            data:{
                newPenalty:await savePenalty(data),
                total:await getTotalPenalty({schoolId,yearId})
            }
        }
    }
    this.updatePenalty = async({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {amount,reason,remark,penalty_id} = body

        let query = {
            schoolId,yearId,
            _id:penalty_id
        }
        if(await getTotalPenalty(query) == 0){
            TE("Cannot Find Penalty")
        }

        const data = {}

        if(amount) data.amount = amount
        if(reason) data.reason = reason
        if(remark) data.remark = remark

       return {
            data:{
                updatePenalty:await savePenalty(data,penalty_id),
                total:await getTotalPenalty({schoolId,yearId})
            }
        }
    }
    this.deletePenalty = async ({body,decoded,params}) => {
        const {schoolId,yearId} = decoded
        const {PenaltyId} = params
        if(await getTotalPenalty({_id:PenaltyId})  == 0){
            TE("Cannot Find Penalty")
        }
        return {
            data:{
                deletedPenalty:await deletePenaltyFromDB(PenaltyId),
                total:await getTotalPenalty({schoolId, yearId})
            }
        }
    }
    this.getAllPenalty = async ({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {skip,limit} = getSkipLimitFromBody(body)

        const filterValues = [
            "from","to","student_id","penalty_id","is_paid"
        ]
        const {from,to,penalty_id,student_id,is_paid} = getFilterValues(filterValues,body)

        let query = {}
        query.yearId = yearId
        query.schoolId = schoolId

        if( penalty_id != null ) query._id = penalty_id
        if( is_paid != null ) query.is_paid = is_paid
        if( student_id != null ) query.student_id = student_id

        if(from != null && to != null ) {
            query.updatedAt = {
                $gte: from, 
                $lt: to
            }
        }


        const all_data = await getPenaltyData(query,skip,limit)

        return {
            data:{
                all_data,
                totalPage:all_data.length,
                total:await getTotalPenalty(query),
                pagination:{
                    skip,
                    limit
                },
                filter:{
                    from,to,penalty_id,student_id,is_paid
                }
            }
        }
    }
    return this;
  })();