const School = require('../../models/school')


module.exports = (function () {

    /**
     * Create new School
     * @param {*} param0 
     */
    this.createSchool = async({body}) => {
        const {schoolName,address,logo,numberOfStudent,noOfTeacher,city,district,state} = body

        if(!schoolName || !address || !numberOfStudent || !noOfTeacher || !logo){
            TE("Invalid Parameter: schoolName, address, numberOfStudent, noOfTeacher, logo is required")
        }
        const newSchool = await School.create({
            schoolName,address,logo,numberOfStudent,noOfTeacher,city,district,state
        })
        return {
            data:{
                newSchool,
                total:await School.countDocuments()
            }
        };
    }

    this.updateSchool = async({body}) => {
        const {schoolId,schoolName,address,logo,numberOfStudent,noOfTeacher,city,district,state} = body
        if(!schoolId){
            TE("Invalid Parameter: schoolId is required")
        }
        const school = await School.findById(schoolId)
        if(school == null){
            TE("Cannot find School")
        }

        if(schoolName){
            school.schoolName = schoolName
        }
        if(address){
            school.address = address
        }
        if(logo){
            school.logo = logo
        }
        if(numberOfStudent){
            school.numberOfStudent = numberOfStudent
        }
        if(noOfTeacher){
            school.noOfTeacher = noOfTeacher
        }
        if(city){
            school.city = city
        }
        if(district){
            school.district = district
        }
        if(state){
            school.state = state
        }
        
        const updateSchool = await school.save()

        return {
            data:{
                updateSchool
            }
        }

    }

    this.deleteSchool = async({params}) => {
        const {schoolId} = params

        if(!verifyObjectId(schoolId)){
            TE("Invalid School Id")
        }

        const school = await School.findById(schoolId)

        if(school.delete() == null){
            TE("Error Occured in Process")
        }

        return {
            data:{
                message:"School Deleted !!"
            }
        }
    }

    this.getAllSchool = async({body}) => {
        const {city,state,district,schoolName,skip,limit} = body

        const searchParams = {}

        if(city){
            searchParams['city'] = city
        }
        if(state){
            searchParams['state'] = state
        }
        if(district){
            searchParams['district'] = district
        }
        if(schoolName){
            searchParams['schoolName'] = new Reg(schoolName,'i')
        }

        const school = await School.find(searchParams).skip(skip).limit(limit)
        return {
            data:{
                school,
                totalCountPage: school.length,
                total: await School.countDocuments()
            }
        }
    }
    this.getIndividualSchool = async({body,decoded}) => {
        const {schoolId} = decoded
        const school = await School.findById(schoolId);
        return {
            data:{
                decoded
            }
        }
    }
    return this;
  })();