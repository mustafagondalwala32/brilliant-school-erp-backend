const {
    getTotalEnquiry,
    saveEnquiry,
    deleteEnquiryFromDB,
    getEnquiryData
} = require("../../helper/enquiry/enquiryHelper")


module.exports = (function () {
    this.createEnquiry = async ({body,decoded}) => {
        const {schoolId,yearId} = decoded
        const {typeOther,type,date,name,fatherName,address,contactNo1,contactNo2,nextFollowDate,enquiryRemark1,enquiryRemark2} = body
        if(!type || !date || !name  || !contactNo1 || !enquiryRemark1){
            TE("Invalid Parameter: type,date,name,contactNo1,enquiryRemark1 is required")
        }
        // if type is 3 than check for type other
        if(type == 3) 
            if(!typeOther) TE("Invalid Parameter: typeOther is required")
        else
            // if type is not equal to 3 than typeOther should be ""
            typeOther = ""


        // make the saving array
        const data = {
            schoolId,yearId,type,date,name,fatherName,address,contactNo1,contactNo2,nextFollowDate,enquiryRemark1,enquiryRemark2
        }

        // save data and send to response
        return {
            data:{
                newEnquiry:await saveEnquiry(data),
                total:await getTotalEnquiry({schoolId,yearId})
            }
        }
    }
    this.updateEnquiry = async({body,decoded}) => {
        // get all the parameters
        const {schoolId,yearId} = decoded
        const {enquiry_id,typeOther,type,date,name,fatherName,address,contactNo1,contactNo2,nextFollowDate,enquiryRemark1,enquiryRemark2} = body


        // get the data from db
        let query = {
            _id:enquiry_id
        }
        
        // check is already exist
        if(await getTotalEnquiry(query) == 0){
            TE("Cannot Find Enquiry")
        }

        const data = {}
        
        if(!typeOther) data.typeOther = typeOther
        if(!type) data.type = type
        if(!date) data.date = date
        if(!name) data.name = name
        if(!fatherName) data.fatherName = fatherName
        if(!address) data.address = address
        if(!contactNo1) data.contactNo1 = contactNo1
        if(!contactNo2) data.contactNo2 = contactNo2
        if(!nextFollowDate) data.nextFollowDate = nextFollowDate
        if(!enquiryRemark1) data.enquiryRemark1 = enquiryRemark1
        if(!enquiryRemark2) data.enquiryRemark2 = enquiryRemark2

       return {
            data:{
                updateEnquiry:await saveEnquiry(data,enquiry_id),
                total:await getTotalEnquiry({schoolId,yearId})
            }
        }
    }
    this.deleteEnquiry = async ({body,decoded,params}) => {
        const {schoolId,yearId} = decoded
        const {enquiryId} = params
        if(await getTotalEnquiry({_id:enquiryId})  == 0){
            TE("Cannot Find Enquiry")
        }
        return {
            data:{
                deletedEnquiry:await deleteEnquiryFromDB(enquiryId),
                total:await getTotalEnquiry({schoolId, yearId})
            }
        }
    }
    this.getAllEnquiry = async ({body,decoded}) => {
        // get the schoolId and year Id 
        const {schoolId,yearId} = decoded

        // santilize the skip and limit
        const {skip,limit} = getSkipLimitFromBody(body)

        // define the filter values
        const filterValues = [
            "from","to","name","enquiry_id","type"
        ]

        // santize the filter values
        const {from,to,enquiry_id,type,name} = getFilterValues(filterValues,body)

        // make query
        let query = {}
        query.yearId = yearId
        query.schoolId = schoolId

        if( enquiry_id != null ) query._id = enquiry_id
        if( type != null ) query.type = type
        if( name != null ) query.name = new RegExp(name,'i')


        if(from != null && to != null ) {
            query.updatedAt = {
                $gte: from, 
                $lt: to
            }
        }

        // get the data
        const all_data = await getEnquiryData(query,skip,limit)

        return {
            data:{
                all_data,
                totalPage:all_data.length,
                total:await getTotalEnquiry(query),
                totalCount:await getTotalEnquiry({schoolId,yearId}),
                pagination:{
                    skip,
                    limit
                },
                filter:{
                    from,to,enquiry_id,type,name
                }
            }
        }
    }
    return this;
  })();