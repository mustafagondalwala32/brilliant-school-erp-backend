const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const feeInstallmentSchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      installmentName:{
          type:String,
          required:[true,"Fee Installment Name is required"],
      },
      yearId:{
          type:Number,
          required:[true,"Year Id is required"]
      },
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("FeeInstallment", feeInstallmentSchema)