const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const teacherTimeTableSchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      yearId:{
        type:Number,
        required:[true,"Year Id is required"]
      },
      teacherId:{
        type:Schema.Types.ObjectId,
        ref:'Teacher',
        required:[true,"Teacher Id is required"]
      },
      status:{
        type:Number,
        default:0
      },
      name:{
          type:Schema.Types.String,
          required:[true,"Name is required"]
      },
      timetableStructure:{
        "monday":[
          {
            "periodId":{
              type:Schema.Types.ObjectId,
              ref:'Period'
            },
            "subjectId":{
              type:Schema.Types.ObjectId,
              ref:'Subject'
            },
          }
        ],
        "tuesday":[
          {
            "periodId":{
              type:Schema.Types.ObjectId,
              ref:'Period'
            },
            "subjectId":{
              type:Schema.Types.ObjectId,
              ref:'Subject'
            }
          }
        ],
        "wednesday":[
          {
            "periodId":{
              type:Schema.Types.ObjectId,
              ref:'Period'
            },
            "subjectId":{
              type:Schema.Types.ObjectId,
              ref:'Subject'
            }
          }
        ],
        "thursday":[
          {
            "periodId":{
              type:Schema.Types.ObjectId,
              ref:'Period'
            },
            "subjectId":{
              type:Schema.Types.ObjectId,
              ref:'Subject'
            }
          }
        ],
        "friday":[
          {
            "periodId":{
              type:Schema.Types.ObjectId,
              ref:'Period'
            },
            "subjectId":{
              type:Schema.Types.ObjectId,
              ref:'Subject'
            }
          }
        ],
        "saturday":[
          {
            "periodId":{
              type:Schema.Types.ObjectId,
              ref:'Period'
            },
            "subjectId":{
              type:Schema.Types.ObjectId,
              ref:'Subject'
            }
          }
        ]
      }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("TeacherTimeTable", teacherTimeTableSchema)