const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const transactionSchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      yearId:{
        type:Number,
        required:[true,"Year Id is required"]
      },
      type:{
          /**
           * 1) For Debit
           * 2) For Credit
           */
          type:Number,
          required:[true,"Type is required"]
      },
      partyType:{
        type:Number,
        /**
         * 1) Student
         * 2) Staff
         * 3) Other
         */
        required:[true,"Party Type is required"]
      },
      date:{
          type:Date,
          required:[true,"Date is required"]
      },
      name:{
          type:String,
          default:""
      },
      address:{
          type:String,
          default:""
      },
      contact_no:{
          type:String,
          required:[true,"Contact No is required"]
      },
      totalAmount:{
          type:Number,
          required:[true,"Total Amount is required"]
      },
      paymentMode:{
          type:Number,
          /**
           * 1) Cash
           * 2) Cheque
           * 3) Online
           */
          required:[true,"Payment Mode is required"]
      },
      remark:{
          type:String,
          required:[true,"Remark is required"]
      },
      billNo:{
          type:String,
          default:""
      },
      billDate:{
          type:Date,
          default:null
      },
      billImage:{
          type:String,
          default:""
      }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("Transaction", transactionSchema)