const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const teacherSchema = new Schema(
  {
    schoolId:{
        type:Schema.Types.ObjectId,
        ref:'Student',
        require:[true,"School Id is required"]
    },
    yearId:{
        type:Number,
        required:[true,"Year Id is required"]
    },
    empid:{
        type: String,
        required:[true,"EmpId is Required"],
        unique: [true,"EmpId is Must be Unique"]
    },
    firstname:{
        type: String,
        required:[true,"Name is Required"]
    },
    lastName:{
        type: String,
        required:[true,"Last Name is Required"]
    },
    middleName:{
        type:String,
        default:""
    },
    email:{
        type:String,
        default:""
    },
    gender:{
        type:Number,
        default:null
    },
    phoneNumber:{
        type:String,
        default:null
    },
    address:{
        type:String,
        default:""
    },
    qualification:{
        type: String,
        default:""
    },
    dob:{
        type:Date,
        default:null
    },
    bloodGroup:{
        type:Number,
        default:null
    },
    dateOfJoining:{
        type:Date,
        default:null
    },
    documentId:{
        type:Schema.Types.ObjectId,
        ref:'TeacherDocument',
        required:[true,"Teacher Document is required"]
    },
    userId:{
        type:Schema.Types.ObjectId,
        ref:'User',
        required:[true,"User Id is required"]
    },
    assignedClassId:{
        type:Schema.Types.ObjectId,
        ref:'Class',
        default:null
    }
  },
  { timestamps: true },
  { minimize: false }
);


teacherSchema.index({ empid: 1, schoolId: 1, yearId:1 }, { unique: true })


module.exports = mongoose.model("Teacher", teacherSchema)