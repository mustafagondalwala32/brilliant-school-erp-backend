const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const busSchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      name:{
          type:String,
          required:[true,"Bus Name is required"],
      },
      company:{
          type:String,
          required:[true,"Company Name is required"],
      },
      model_no:{
          type:String,
          required:[true,"Model Name is required"]
      },
      bus_no:{
          type:String,
          required:[true,"Bus No is required"]
      },
      owner_name:{
          type:String,
          required:[true,"Owner Name is required"]
      },
      owner_contact:{
        type:String,
        required:[true,"Owner Contact is required"]
      },
      register_no:{
          type:String,
          required:[true,"Register No is required"]
      },
      capacity:{
          type:Number,
          default:null
      },
      document_images:{
          photo:{
              type:String,
              default:""
          },
          registeration_card:{
              type:String,
              default:""
          },
          insurance_card:{
              type:String,
              default:""
          },
          pollution_certificate:{
              type:String,
              default:""
          },
          fitness_certificate:{
              type:String,
              default:""
          },
          permit_certificate:{
              type:String,
              default:""
          },
          speed_certificate:{
              type:String,
              default:""
          },
          camera_certificate:{
              type:String,
              default:""
          },
          other_document_1:{
            type:String,
            default:""
          },
          other_document_2:{
            type:String,
            default:""
          },
      }
        
    
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("Bus", busSchema)