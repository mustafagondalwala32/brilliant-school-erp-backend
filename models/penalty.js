const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PenaltySchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      yearId:{
        type:Number,
        required:[true,"Year Id is required"]
      },
      student_id:{
          type:String,
          required:[true,"Name is required"]
      },
      amount:{
          type:Number,
          required:[true,"Amount is required"]
      },
      reason:{
          type:String,
          default:""
      },
      remark:{
          type:String,
          default:""
      },
      is_paid:{
          type:Boolean,
          default:0
      }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("Penalty", PenaltySchema)