const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const crypto = require('crypto');

const userSchema = new Schema(
  {
     name:{
         type: String,
         default:""
     },
     email:{
         type: String,
         default:""
     },
     loginText:{
         type: String,
         required:[true,"Login Text is Required"]
     },
     hash:{
        type: String,
        required:[true,"Password is Required"]
     },
     salt:{
        type: String,
        required:[true,"Salt is Required"]
     },
     userType:{
         type: Number,
         /**
          * 1) Admin
          * 2) Sub-Admin
          * 3) Teacher
          * 4) Parent
          * 5) Student
          */
         required:[true,"User Type is Required"]
     },
     schoolId:{
        type: Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is Required"]
     },
     yearId:{
         type:Number,
         required:[true,"Year Id is Required"]
     }
  },
  { timestamps: true },
  { minimize: false }
);

userSchema.index({ loginText: 1, schoolId: 1,yearId: 1}, { unique: true })


userSchema.methods.setPassword = function(password) {
    // Creating a unique salt for a particular user
       this.salt = crypto.randomBytes(16).toString('hex');
       this.hash = crypto.pbkdf2Sync(password, this.salt, 
       1000, 64, `sha512`).toString(`hex`);
};

userSchema.methods.validPassword = function(password) {
    var hash = crypto.pbkdf2Sync(password, 
    this.salt, 1000, 64, `sha512`).toString(`hex`);
    return this.hash === hash;
};
  
   
module.exports = mongoose.model("User", userSchema)