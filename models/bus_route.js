const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const busRouteSchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      yearId:{
          type:Number,
          required:true
      },
      route_name:{
          type:String,
          required:[true,"Route Name is required"]
      },
      assigned_bus:{
        type:Schema.Types.ObjectId,
        ref:'Bus',
        default:null
      },
      stops:[
          {
            stop_name:{
                type:String,
                default:""
            },
            time:{
                type:String,
                default:""
            }
        }
      ]
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("BusRoute", busRouteSchema)