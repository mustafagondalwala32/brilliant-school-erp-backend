const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const staffSchema = new Schema(
  {
        schoolId:{
            type:Schema.Types.ObjectId,
            ref:'Student',
            require:[true,"School Id is required"]
        },
        employee_id:{
            type:String,
            required:[true,"Employee Id is required"]
        },
        employee_name:{
            type:String,
            required:[true,"Employee Name is required"]
        },
        gender:{
            type:Number,
            required:[true,"Gender is required"]
        },
        dob:{
            type:Date,
            required:[true,"Date of Birth is required"]
        },
        relativeName:{
            type:String,
            required:[true,"Relative Name is required"]
        },
        email:{
            type:String,
            default:""
        },
        contactNo:{
            type:String,
            unique:[true,"Contact is already in system"],
            required:[true,"Contact No is required"]
        },
        address:{
            type:String,
            default:""
        },
        qualification:{
            type:String,
            default:""
        },
        bloodgroup:{
            type:String,
            default:""
        },
        document_files:{
            employee_photo:{
                type:String,
                default:null
            },
            experience_letter:{
                type:String,
                default:null
            },
            qualification_proof:{
                type:String,
                default:null
            },
            id_proof:{
                type:String,
                default:null
            },
            other_document_1:{
                type:String,
                default:null
            },
            other_document_2:{
                type:String,
                default:null
            }
        },
        date_of_joining:{
            type:Date,
            required:[true,"Date of Joining is required"]
        },
        designation:{
            type:String,
            required:[true,"Designation is required"]
        },
        salary:{
            amount:{
                type:Number,
                default:null,
                required:[true,"Salary is Required"]
            },
            pfNo:{
                type:String,
                default:""
            },
            pfAmount:{
                type:Number,
                default:0
            },
            tdsAmount:{
                type:Number,
                default:0
            },
            professionalTaxAmount:{
                type:Number,
                default:0
            },
            daAmount:{
                type:Number,
                default:0
            },
            hraAmount:{
                type:Number,
                default:0
            },
            totalAmount:{
                type:Number,
                default:0
            },
            salaryRemark:{
                type:String,
                default:""
            }
        },
        leave:{
                cascualLeave:{
                    type:Number,
                    default:0
                },
                payEarnLeave:{
                    type:Number,
                    default:0
                },
                sickLeave:{
                    type:Number,
                    default:0
                },
                otherLeave:{
                    type:Number,
                    default:0
                }
        }
  },
  { timestamps: true },
  { minimize: false }
);


staffSchema.index({ schoolId: 1, employee_id:1 }, { unique: true })


module.exports = mongoose.model("Staff", staffSchema)