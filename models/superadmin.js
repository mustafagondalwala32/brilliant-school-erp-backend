const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const superAdminSchema = new Schema(
  {
    adminName:{
        type: String,
        require:[true,"Admin Name is Required"]
    },
    adminEmail:{
        type: String,
        require:[true,"Admin Email is Required"],
        unique: true
    },
    hash:{
        type: String,
        require:[true,"Admin Hash is Required"]
    },
    salt:{
      type: String,
      required:[true,"Salt is Required"]
    },
    loginToken:{
      type: String,
      default:""
    },
    resetPasswordToken: String,
    resetPasswordExpires: Date
  },
  { timestamps: true },
  { minimize: false }
);


superAdminSchema.methods.setPassword = function(password) {
  // Creating a unique salt for a particular user
     this.salt = crypto.randomBytes(16).toString('hex');
     this.hash = crypto.pbkdf2Sync(password, this.salt, 
     1000, 64, `sha512`).toString(`hex`);
};

superAdminSchema.methods.validPassword = function(password) {
  var hash = crypto.pbkdf2Sync(password, 
  this.salt, 1000, 64, `sha512`).toString(`hex`);
  return this.hash === hash;
};

module.exports = mongoose.model("SuperAdmin", superAdminSchema)