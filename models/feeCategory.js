const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const feeCategorySchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      categoryName:{
          type:String,
          required:[true,"Fee Category Name is required"],
      },
      yearId:{
          type:Number,
          required:[true,"Year Id is required"]
      },
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("FeeCategory", feeCategorySchema)