const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schoolPeriodSchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      yearId:{
          type:Number,
          required:[true,"Year Id is required"]
      },
      name:{
          type:String,
          required:[true,"Period Name is required"]
      },
      startTime:{
          type:String,
          required:[true,"Period Start Time is required"]
      },
      endTime:{
          type:String,
          required:[true,"Period End Time is required"]
      }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("SchoolPeriod", schoolPeriodSchema)