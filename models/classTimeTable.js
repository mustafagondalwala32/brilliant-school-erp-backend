const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const classTimeTableSchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      yearId:{
        type:Number,
        required:[true,"Year Id is required"]
      },
      classId:{
        type:Schema.Types.ObjectId,
        ref:'Class',
        required:[true,"Class Id is required"]
      },
      status:{
        type:Number,
        default:0
      },
      name:{
          type:Schema.Types.String,
          required:[true,"Name is required"]
      },
      timetableStructure:{
        "monday":[
          {
            "periodId":{
              type:Schema.Types.ObjectId,
              ref:'Period'
            },
            "subjectId":{
              type:Schema.Types.ObjectId,
              ref:'Subject'
            },
            "teacherId":{
              type:Schema.Types.ObjectId,
              ref:'Teacher'
            },
          }
        ],
        "tuesday":[
          {
            "periodId":{
              type:Schema.Types.ObjectId,
              ref:'Period'
            },
            "subjectId":{
              type:Schema.Types.ObjectId,
              ref:'Subject'
            },
            "teacherId":{
              type:Schema.Types.ObjectId,
              ref:'Teacher'
            },
          }
        ],
        "wednesday":[
          {
            "periodId":{
              type:Schema.Types.ObjectId,
              ref:'Period'
            },
            "subjectId":{
              type:Schema.Types.ObjectId,
              ref:'Subject'
            },
            "teacherId":{
              type:Schema.Types.ObjectId,
              ref:'Teacher'
            },
          }
        ],
        "thursday":[
          {
            "periodId":{
              type:Schema.Types.ObjectId,
              ref:'Period'
            },
            "subjectId":{
              type:Schema.Types.ObjectId,
              ref:'Subject'
            },
            "teacherId":{
              type:Schema.Types.ObjectId,
              ref:'Teacher'
            },
          }
        ],
        "friday":[
          {
            "periodId":{
              type:Schema.Types.ObjectId,
              ref:'Period'
            },
            "subjectId":{
              type:Schema.Types.ObjectId,
              ref:'Subject'
            },
            "teacherId":{
              type:Schema.Types.ObjectId,
              ref:'Teacher'
            },
          }
        ],
        "saturday":[
          {
            "periodId":{
              type:Schema.Types.ObjectId,
              ref:'Period'
            },
            "subjectId":{
              type:Schema.Types.ObjectId,
              ref:'Subject'
            },
            "teacherId":{
              type:Schema.Types.ObjectId,
              ref:'Teacher'
            },
          }
        ]
      }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("ClassTimeTable", classTimeTableSchema)