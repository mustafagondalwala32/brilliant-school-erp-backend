const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const registerStudentSchema = new Schema(
  {
      schoolId:{
            type:Schema.Types.ObjectId,
            ref:'Student',
            require:[true,"School Id is required"]
      },
      yearId:{
        type:Number,
        required:[true,"Year Id is required"]
      },
      classId:{
        type:Schema.Types.ObjectId,
        ref:'Class',
        require:[true,"Class Id is required"]
      },
      registerNo:{
          type:String,
          default:''
      },
      studentName:{
          type:String,
          required:[true,"Student Name is required"]
      },
      fatherName:{
        type:String,
        default:""
      },
      motherName:{
        type:String,
        default:""
      },
      lastName:{
        type:String,
        required:[true,"Last Name is required"]
      },
      parentContactNo1:{
          type:String,
          required:[true,"Parent Contact No is required"]
      },
      parentContactNo2:{
        type:String,
        default:""
      },
      dob:{
          type:Date,
          required:[true,"Dob is required"]
      },
      gender:{
        type:Number,
        /** 
         * 1) Male, 2) Female 3) Other */
        required:[true,"Gender is required"]  
      },
      address:{
        type:String,
        required:[true,"Address is required"]
      },
      block:{
        type:String,
        default:""
      },
      district:{
        type:String,
        default:""
      },
      state:{
        type:String,
        default:""
      },
      pincode:{
          type:String,
          default:""
      },
      student_photo:{
          type:String,
          default:String
      },
      mother_photo:{
        type:String,
        default:String
      },
      father_photo:{
            type:String,
            default:String
      }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("RegisterStudent", registerStudentSchema)