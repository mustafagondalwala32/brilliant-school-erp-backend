const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const classWiseFeeSchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      yearId:{
        type:Number,
        required:[true,"Year Id is required"]
      },
      classId:{
        type:Schema.Types.ObjectId,
        ref:'Class',
        required:[true,"Class Id is required"]
      },
      feeStructure:[{
        installmentId:{
            type:Schema.Types.ObjectId,
            ref:'FeeInstallment',
        },
        "categoryWise":[
            {
                "categoryId":{
                    type:Schema.Types.ObjectId,
                    ref:'FeeCategory',
                },
                "amount":{
                    type:Number,
                    default:null
                }
            }
        ]
      }]
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("ClassWiseFee", classWiseFeeSchema)