const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const eventSchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      yearId:{
        type:Number,
        required:[true,"Year Id is required"]
      },
      name:{
          type:String,
          required:[true,"Name is required"]
      },
      from:{
          type:Date,
          required:[true,"from is required"]
      },
      to:{
        type:Date,
        required:[true,"to is required"]
      },
      description:{
          type:String,
          default:""
      },
      classIds:[{
        type:Schema.Types.ObjectId,
        ref:'Class'
      }]
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("Event", eventSchema)