const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const enquirySchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      yearId:{
        type:Number,
        required:[true,"Year Id is required"]
      },
      type:{
          /**
           * 1) For Admission
           * 2) For Job
           * 3) For Other
           */
          type:Number,
          required:[true,"Type is required"]
      },
      typeOther:{
        type:String,
        default:""
      },
      date:{
          type:Date,
          required:[true,"Date is required"]
      },
      name:{
          type:String,
          default:""
      },
      fatherName:{
          type:String,
          default:""
      },
      address:{
          type:String,
          default:""
      },
      contactNo1:{
          type:String,
          required:[true,"Contact No is required"]
      },
      contactNo2:{
          type:String,
          default:""
      },
      nextFollowDate:{
          type:Date,
          default:null
      },
      enquiryRemark1:{
          type:String,
          required:[true,"Remark is required"]
      },
      enquiryRemark2:{
          type:String,
          default:""
      }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("Enquiry", enquirySchema)