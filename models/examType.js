const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const examTypeSchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      yearId:{
        type:Number,
        required:[true,"Year Id is required"]
      },
      name:{
          type:String,
          required:[true,"Name is required"]
      },
      status:{
          type:Number,
          default:0
      }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("ExamType", examTypeSchema)