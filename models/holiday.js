const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const holidaySchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      yearId:{
        type:Number,
        required:[true,"Year Id is required"]
      },
      name:{
          type:String,
          required:[true,"Name is required"]
      },
      date:{
          type:Date,
          required:[true,"Date is required"]
      },
      description:{
          type:String,
          default:""
      }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("Holiday", holidaySchema)