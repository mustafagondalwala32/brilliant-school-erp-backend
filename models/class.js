const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const classSchema = new Schema(
  {
      schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
      },
      className:{
          type:String,
          required:[true,"Class Name is required"],
      },
      section:{
          type:String,
          default:""
      },
      yearId:{
          type:Number,
          required:[true,"Year Id is required"]
      },
      rollNoString:{
        type:String,
        default:""
      },
      currentRollNo:{
        type:Number,
        default:null
      },
      assignTeacherId:{
        type:Schema.Types.ObjectId,
        ref:'Teacher',
        default:null
      },
      feeInstallment:
        [
            {
              installment:{
                type:Schema.Types.ObjectId,
                ref:'FeeInstallment',
                default:null
              },
              amount:{
                type:Number,
                default:null
              }
            }
      ],
      subjects:[
        {
          type:Schema.Types.ObjectId,
          ref:'Subject',
        }
      ]
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("Class", classSchema)