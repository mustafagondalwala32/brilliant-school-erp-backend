const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const studentSchema = new Schema(
  {
        schoolId:{
            type:Schema.Types.ObjectId,
            ref:'Student',
            require:[true,"School Id is required"]
        },
        yearId:{
            type:Number,
            required:[true,"Year Id is required"]
        },
        classId:{
            type:Schema.Types.ObjectId,
            ref:'Class',
            require:[true,"Class Id is required"]
        },
        rollNo:{
            type:String,
            required:[true,"Roll No is required"]
        },
        studentName:{
            type:String,
            required:[true,"Student Name is required"]
        },
        fatherName:{
            type:String,
            default:""
        },
        motherName:{
            type:String,
            default:""
        },
        lastName:{
            type:String,
            required:[true,"Last Name is required"]
        },
        guardianName:{
            type:String,
            default:""
        },
        handicapped:{
            type:Number,
            default:0
        },
        parentContactNo1:{
            type:String,
            required:[true,"Parent Contact No is required"]
        },
        parentContactNo2:{
            type:String,
            default:""
        },
        parentEmail:{
            type:String,
            default:""
        },
        dob:{
            type:Date,
            required:[true,"Dob is required"]
        },
        age:{
            type:Number,
            default:""
        },
        gender:{
            type:Number,
            /** 
             * 1) Male, 2) Female 3) Other */
            required:[true,"Gender is required"]  
        },
        address:{
            type:String,
            required:[true,"Address is required"]
        },
        block:{
            type:String,
            default:""
        },
        district:{
            type:String,
            default:""
        },
        state:{
            type:String,
            default:""
        },
        pincode:{
            type:String,
            default:""
        },
        religion:{
            type:Number,
            required:[true,"Religion is required"]
        },
        caste:{
            type:Number,
            required:[true,"Caste is required"]
        },
        student_photo:{
            type:String,
            default:""
        },
        documentId:{
            type:Schema.Types.ObjectId,
            ref:'StudentDocument',
            default:null
        },
        userId:{
            type:Schema.Types.ObjectId,
            ref:'User'
        }
  },
  { timestamps: true },
  { minimize: false }
);


studentSchema.index({ rollNo: 1, schoolId: 1, yearId:1 }, { unique: true })


module.exports = mongoose.model("Student", studentSchema)