const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const teacherSchema = new Schema(
  {
      teacherId:{
        type:Schema.Types.ObjectId,
        ref:'Teacher'
      },
      empid:{
          type:String,
          required:[true,"Emp Id is Required"]
      },
      aadharCard:{
          type:String,
          default:""
      },
      bankName:{
          type:String,
          default:""
      },
      bankNo:{
          type:String,
          default:""
      },
      pandCardNo:{
          type:String,
          default:""
      },
      document:{
        teacherPhoto:{
            type:String,
            default:""
        },
        experienceLetter:{
            type:String,
            default:""
        },
        idProof:{
            type:String,
            default:""
        },
        otherDocuments1:{
            type:String,
            default:""
        },
        otherDocuments2:{
            type:String,
            default:""
        },
      },
      salary:{
            amount:{
                type:Number,
                default:null,
                required:[true,"Salary is Required"]
            },
            pfNo:{
                type:String,
                default:""
            },
            pfAmount:{
                type:Number,
                default:0
            },
            tdsAmount:{
                type:Number,
                default:0
            },
            professionalTaxAmount:{
                type:Number,
                default:0
            },
            daAmount:{
                type:Number,
                default:0
            },
            hraAmount:{
                type:Number,
                default:0
            },
            salaryRemark:{
                type:String,
                default:""
            }
      },
      leave:{
            cascualLeave:{
                type:Number,
                default:0
            },
            payEarnLeave:{
                type:Number,
                default:0
            },
            sickLeave:{
                type:Number,
                default:0
            },
            otherLeave:{
                type:Number,
                default:0
            }
      }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("TeacherDocument", teacherSchema)