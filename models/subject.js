const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const subjectSchema = new Schema(
  {
    schoolId:{
        type:Schema.Types.ObjectId,
        ref:'School',
        required:[true,"School Id is required"],
    },
    yearId:{
        type:Number,
        required:[true,"Year Id is required"]
    },
    subjectName:{
        type:String,
        required:[true,"Subject Name is required"]
    },
    status:{
      type:Number,
      default:1
    }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("Subject", subjectSchema)