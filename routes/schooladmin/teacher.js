const express = require("express");
const router = express.Router();
const {
    createNewTeacher,
    getAllTeacher,
    getTeacher,
    searchTeacher
} = require("../../controller/teacherController")

router.post('/create',createNewTeacher)
router.post('/fetchAll',getAllTeacher)
router.get('/search',searchTeacher)
router.get('/:teacherId',getTeacher)


module.exports = router
