const express = require("express");
const router = express.Router();

const {
    createStaff,
    updateStaff,
    deleteStaff,
    getAllStaff
} = require("../../controller/staffController")

router.post('/create',createStaff)
router.put('/',updateStaff)
router.delete('/:staffId',deleteStaff)
router.post('/fetchAll',getAllStaff)



module.exports = router
