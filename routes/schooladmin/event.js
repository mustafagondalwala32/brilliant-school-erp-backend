const express = require("express");
const router = express.Router();

const {
    createEvent,
    updateEvent,
    deleteEvent,
    getAllEvent
} = require("../../controller/eventController")

router.post('/create',createEvent)
router.put('/',updateEvent)
router.delete('/:eventId',deleteEvent)
router.post('/fetchAll',getAllEvent)



module.exports = router
