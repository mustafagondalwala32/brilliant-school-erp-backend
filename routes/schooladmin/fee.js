const express = require("express");
const router = express.Router();

const {
    createNewFeeCategory,
    updateFeeCategory,
    deleteFeeCategory,
    getAllFeeCategory,
    createNewFeeInstallment,
    getAllFeeInstallment,
    updateFeeInstallment,
    deleteFeeInstallment,
    updateClassWiseFeeInstallment,
    getClassWiseFeeInstallment,
    addFeeToStudent,
    updateStudentFee
} = require("../../controller/feeController")


router.post('/fee-category/create',createNewFeeCategory)
router.put('/fee-category',updateFeeCategory)
router.delete('/fee-category/:feeCategoryId',deleteFeeCategory)
router.post('/fee-category/fetchAll',getAllFeeCategory)


router.post('/installments/create',createNewFeeInstallment)
router.put('/installments',updateFeeInstallment)
router.delete('/installments/:feeInstallmentId',deleteFeeInstallment)
router.post('/installments',getAllFeeInstallment)


router.post('/class-fee-installment-update',updateClassWiseFeeInstallment)
router.post('/class-fee-installment',getClassWiseFeeInstallment)

router.post('/set-fee-classwise',addFeeToStudent)
router.post('/update-fee-structure-student-wise',updateStudentFee)


module.exports = router
