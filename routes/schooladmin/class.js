const express = require("express");
const router = express.Router();
const {
    createNewClass,
    deleteClass,
    fetchAllClass,
    updateClass,
    createNewSection,
    assignTeacherToClass,
    assignSubjectToClass
} = require("../../controller/classController")

router.post('/create',createNewClass)
router.delete('/:classId',deleteClass)
router.put('/',updateClass)
router.post('/fetchAll',fetchAllClass)


router.post('/section/create',createNewSection)
router.put('/assign-teacher',assignTeacherToClass)
router.put('/assign-subjects',assignSubjectToClass)



module.exports = router
