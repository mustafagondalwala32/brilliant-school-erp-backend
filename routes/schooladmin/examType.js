const express = require("express");
const router = express.Router();

const {
    createExamType,
    updateExamType,
    deleteExamType,
    getAllExamType
} = require("../../controller/examTypeController")

router.post('/create',createExamType)
router.put('/',updateExamType)
router.delete('/:examtype_id',deleteExamType)
router.post('/fetchAll',getAllExamType)



module.exports = router
