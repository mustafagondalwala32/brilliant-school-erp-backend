const express = require("express");
const router = express.Router();

const {
    getIndividualSchool
} = require("../../controller/schoolController")

router.get('/',getIndividualSchool)


module.exports = router
