const express = require("express");
const router = express.Router();

const {
    createSubject,
    updateSubject,
    getAllSubject,
    deleteSubject
} = require("../../controller/subjectController")


router.post('/create',createSubject)
router.put('/',updateSubject)
router.post('/fetchAll',getAllSubject)
router.delete('/:subjectId',deleteSubject)





module.exports = router
