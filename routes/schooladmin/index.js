const express = require("express");
const router = express.Router();

const schoolAdminMiddleware = require("../../middleware/schoolAdminToken")

const registerStudentRouter = require('./register-student')
const classRouter = require("./class")
const studentRouter = require("./student")
const teacherRouter = require("./teacher")
const feeRouter = require("./fee")
const subjectRouter = require("./subject")
const periodRouter = require("./period")
const timetableRouter = require("./timetable")
const holidayRouter = require("./holiday")
const eventRouter = require("./event")
const schoolRouter = require("./school")
const staffRouter = require("./staff")
const penaltyRouter = require("./penalty")
const enquiryRouter = require("./enquiry")
const transactionRouter = require("./transaction")
const examTypeRouter  = require("./examType")
const busRouter = require("./bus")

router.use('/register-student',schoolAdminMiddleware,registerStudentRouter)
router.use('/class',schoolAdminMiddleware,classRouter)
router.use('/student',schoolAdminMiddleware,studentRouter)
router.use('/teacher',schoolAdminMiddleware,teacherRouter)
router.use('/fee',schoolAdminMiddleware,feeRouter)
router.use('/subject',schoolAdminMiddleware,subjectRouter)
router.use('/period',schoolAdminMiddleware,periodRouter)
router.use('/timetable',schoolAdminMiddleware,timetableRouter)
router.use('/holiday',schoolAdminMiddleware,holidayRouter)
router.use('/event',schoolAdminMiddleware,eventRouter)
router.use('/school',schoolAdminMiddleware,schoolRouter)
router.use('/staff',schoolAdminMiddleware,staffRouter)
router.use('/penalty',schoolAdminMiddleware,penaltyRouter)
router.use('/enquiry',schoolAdminMiddleware,enquiryRouter)
router.use('/transaction',schoolAdminMiddleware,transactionRouter)
router.use('/exam-type',schoolAdminMiddleware,examTypeRouter)
router.use('/bus',schoolAdminMiddleware,busRouter)




module.exports = router
