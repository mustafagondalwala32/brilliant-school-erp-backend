const express = require("express");
const router = express.Router();

const {
    createTransaction,
    updateTransaction,
    deleteTransaction,
    getAllTransaction
} = require("../../controller/transactionController")

router.post('/add-entry',createTransaction)
router.put('/',updateTransaction)
router.delete('/:transaction_id',deleteTransaction)
router.post('/fetchAll',getAllTransaction)



module.exports = router
