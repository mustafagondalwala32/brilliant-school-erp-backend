const express = require("express");
const router = express.Router();

const {
    createBus,
    updateBus,
    deleteBus,
    getAllBus,
    createBusRoute,
    updateBusRoute,
    deleteBusRoute,
    getAllBusRoute,
    createBusRouteStop,
    deleteBusRouteStop
} = require("../../controller/busController")

router.post('/create',createBus)
router.put('/',updateBus)
router.delete('/:bus_id',deleteBus)
router.post('/fetchAll',getAllBus)


router.post('/route/create',createBusRoute)
router.put('/route',updateBusRoute)
router.post('/route/fetchAll',getAllBusRoute)
router.delete('/route/:busroute_id',deleteBusRoute)


router.post('/route/stop/create',createBusRouteStop)
router.delete('/route/stop/:route_id/:stop_id',deleteBusRouteStop)


module.exports = router
