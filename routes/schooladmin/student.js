const express = require("express");
const router = express.Router();
const {
    createNewStudent,
    fetchAllStudent,
    updateStudent,
    addUpdateStudentRollNo,
    getSearchStudent
} = require("../../controller/studentController")

router.post('/create',createNewStudent)
router.post('/fetchAll',fetchAllStudent)
router.put('/',updateStudent)
router.get('/search',getSearchStudent)



router.post('/roll-no',addUpdateStudentRollNo)
module.exports = router
