const express = require("express");
const router = express.Router();
const {
    createNewRegisterStudent,
    fetchAllRegisterStudent,
    updateRegisterStudent
} = require("../../controller/registerStudentController")

router.post('/create',createNewRegisterStudent)
router.post('/fetchAll',fetchAllRegisterStudent)
router.put('/',updateRegisterStudent)

module.exports = router
