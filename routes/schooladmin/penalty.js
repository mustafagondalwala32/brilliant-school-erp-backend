const express = require("express");
const router = express.Router();

const {
    createPenalty,
    updatePenalty,
    deletePenalty,
    getAllPenalty
} = require("../../controller/penaltyController")

router.post('/create',createPenalty)
router.post('/fetchAll',getAllPenalty)
router.put('/',updatePenalty)
router.delete('/:PenaltyId',deletePenalty)

module.exports = router
