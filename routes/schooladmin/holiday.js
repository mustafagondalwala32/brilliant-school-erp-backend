const express = require("express");
const router = express.Router();

const {
    createHoliday,
    updateHoliday,
    deleteHoliday,
    getAllHoliday
} = require("../../controller/holidayController")

router.post('/create',createHoliday)
router.post('/fetchAll',getAllHoliday)
router.put('/',updateHoliday)
router.delete('/:holidayId',deleteHoliday)

module.exports = router
