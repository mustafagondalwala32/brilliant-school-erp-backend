const express = require("express");
const router = express.Router();

const {
    createTimetable,
    updateTimetable,
    getAllTimetable,
    deleteTimetable,
    getIndividualTimetable,
    createTeacherTimetable,
    updateTeacherTimetable,
    getAllTeacherTimetable
} = require("../../controller/timetableController")


router.post('/create',createTimetable)
router.put('/',updateTimetable)
router.post('/fetchAll',getAllTimetable)
router.delete('/:timetableId',deleteTimetable)
router.get('/:timetableId',getIndividualTimetable)



router.post('/teacher/create',createTeacherTimetable)
router.put('/teacher',updateTeacherTimetable)
router.post('/teacher/fetchAll',getAllTeacherTimetable)


module.exports = router
