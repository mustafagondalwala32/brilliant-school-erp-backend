const express = require("express");
const router = express.Router();

const {
    createNewPeriod,
    updatePeriod,
    getAllPeriods,
    deletePeriod
} = require("../../controller/periodController")


router.post('/create',createNewPeriod)
router.put('/',updatePeriod)
router.post('/fetchAll',getAllPeriods)
router.delete('/:periodId',deletePeriod)


module.exports = router
