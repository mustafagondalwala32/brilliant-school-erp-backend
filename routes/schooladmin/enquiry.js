const express = require("express");
const router = express.Router();

const {
    createEnquiry,
    updateEnquiry,
    deleteEnquiry,
    getAllEnquiry
} = require("../../controller/enquiryController")

router.post('/create',createEnquiry)
router.put('/',updateEnquiry)
router.delete('/:enquiryId',deleteEnquiry)
router.post('/fetchAll',getAllEnquiry)



module.exports = router
