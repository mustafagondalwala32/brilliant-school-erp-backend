const express = require("express");
const router = express.Router();
const superAdminRouter = require("./superadmin")
const userRouter = require('./user');
const schoolAdminRouter = require('./schooladmin');
const uploadRouter =  require('./upload')
const {
    uploadFunction
} = require("../controller/uploadController")

router.use('/superadmin',superAdminRouter)
router.use('/user',userRouter)
router.use('/schooladmin',schoolAdminRouter)
router.use('/upload',uploadRouter)
module.exports = router