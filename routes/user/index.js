const express = require("express");
const router = express.Router();
const {login} = require("../../controller/userController")

router.use('/login',login)

module.exports = router
