const express = require("express");
const router = express.Router();
const authenticationRouter = require("./authentication")
const schoolRouter = require("./school")

const superAdminToken = require("../../middleware/superAdminToken")


const {
    showAllSuperAdmin
} = require("../../controller/superAdminController")

const {
    createSchoolAdmin,
    updateSchoolAdmin
} = require("../../controller/schoolAdminController")


router.use('/school',superAdminToken,schoolRouter)
router.use('/authentication',authenticationRouter)
router.get('/',superAdminToken,showAllSuperAdmin)

router.post('/admin',superAdminToken,createSchoolAdmin)
router.put('/admin',superAdminToken,updateSchoolAdmin)




module.exports = router
