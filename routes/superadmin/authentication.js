const express = require("express");
const router = express.Router();

const superAdminToken = require("../../middleware/superAdminToken")

const superAdminController = require("../../controller/superAdminController")


router.put("/",superAdminToken,superAdminController.updateSuperAdmin)
router.post("/",superAdminController.createSuperAdmin)
router.get("/",superAdminToken,superAdminController.showAllSuperAdmin)

router.post("/login",superAdminController.loginAdmin)
router.post("/logout",superAdminToken,superAdminController.logoutAdmin)
router.post("/forgot-password",superAdminController.forgetPassword)
router.post("/reset-password",superAdminController.resetPassword)





module.exports = router
