const express = require("express");
const router = express.Router();

const superAdminToken = require("../../middleware/superAdminToken")

const {
    createSchool,
    updateSchool,
    deleteSchool,
    getAllSchool
} = require("../../controller/schoolController")


router.post('/create',superAdminToken,createSchool)
router.put('/',superAdminToken,updateSchool)
router.delete('/:schoolId',superAdminToken,deleteSchool)
router.post('/',superAdminToken,getAllSchool)




module.exports = router
