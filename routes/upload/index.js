const express = require("express");
const router = express.Router();
const loginToken = require("../../middleware/loginToken")
const {
    addFileToS3,
    removeFileFromS3
} = require("../../controller/uploadController")

router.post('/add-file-to-s3',loginToken,addFileToS3)
router.post('/remove-file-from-s3',loginToken,removeFileFromS3)




module.exports = router
