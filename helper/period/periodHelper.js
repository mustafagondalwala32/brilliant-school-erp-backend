const Period = require('../../models/schoolPeriod')
module.exports = {    
    savePeriod: async(data,_id = null) => {
        if(_id != null){
            return await Period.findByIdAndUpdate(_id,data, {new: true})
        }else{
            return await Period.create(data)
        }
    },
    getTotal: async(query) => {
        return await Period.find(query).countDocuments()
    },
    getData: async(query,skip,limit) => {
        return await Period.find(query).skip(skip).limit(limit)
    },
    deletePeriodFromDB: async(_id) => {
        return await Period.findByIdAndDelete(_id)
    }
}