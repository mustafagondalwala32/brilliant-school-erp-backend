const Student = require('../../models/student')
module.exports = {    
    saveStudent: async(data,_id) => _id != null ? await Student.findByIdAndUpdate(_id,data, {new: true}) : await Student.create(data),
    getTotalStudent: async(query) => await Student.find(query).countDocuments(),
    getStudentData: async(query,skip,limit) => await Student.find(query).sort('-updatedAt').skip(skip).limit(limit),
    deleteStudentFromDB: async(_id) => await Student.findByIdAndDelete(_id)
}