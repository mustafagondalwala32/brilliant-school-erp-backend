const Staff = require('../../models/staff')
module.exports = {    
    saveStaff: async(data,_id) => _id != null ? await Staff.findByIdAndUpdate(_id,data, {new: true}) : await Staff.create(data),
    getTotalStaff: async(query) => await Staff.find(query).countDocuments(),
    getStaffData: async(query,skip,limit) => await Staff.find(query).sort('-updatedAt').skip(skip).limit(limit),
    deleteStaffFromDB: async(_id) => await Staff.findByIdAndDelete(_id)
}