const Period = require('../../models/schoolPeriod')
const Holiday = require('../../models/holiday')
module.exports = {    
    saveHolidayPeriod: async(data,_id) => _id != null ? await Holiday.findByIdAndUpdate(_id,data, {new: true}) : await Holiday.create(data),
    getTotalHoliday: async(query) => await Holiday.find(query).countDocuments(),
    getHolidayData: async(query,skip,limit) => await Holiday.find(query).sort('-updatedAt').skip(skip).limit(limit),
    deleteHolidayFromDB: async(_id) => await Holiday.findByIdAndDelete(_id)
}