const Class = require('../../models/class')
module.exports = {    
    saveClass: async(data,_id) => _id != null ? await Class.findByIdAndUpdate(_id,data, {new: true}) : await Class.create(data),
    getTotalClass: async(query) => await Class.find(query).countDocuments(),
    getClassData: async(query,skip,limit) => await Class.find(query).sort('-updatedAt').skip(skip).limit(limit),
    deleteClassFromDB: async(_id) => await Class.findByIdAndDelete(_id)
}