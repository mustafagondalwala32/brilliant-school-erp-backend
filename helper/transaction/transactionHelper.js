const Transaction = require('../../models/transaction')
module.exports = {    
    saveTransaction: async(data,_id) => _id != null ? await Transaction.findByIdAndUpdate(_id,data, {new: true}) : await Transaction.create(data),
    getTotalTransaction: async(query) => await Transaction.find(query).countDocuments(),
    getTransactionData: async(query,skip,limit) => await Transaction.find(query).sort('-updatedAt').skip(skip).limit(limit),
    deleteTransactionFromDB: async(_id) => await Transaction.findByIdAndDelete(_id)
}