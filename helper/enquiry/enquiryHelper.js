const Enquiry = require('../../models/enquiry')
module.exports = {    
    saveEnquiry: async(data,_id) => _id != null ? await Enquiry.findByIdAndUpdate(_id,data, {new: true}) : await Enquiry.create(data),
    getTotalEnquiry: async(query) => await Enquiry.find(query).countDocuments(),
    getEnquiryData: async(query,skip,limit) => await Enquiry.find(query).sort('-updatedAt').skip(skip).limit(limit),
    deleteEnquiryFromDB: async(_id) => await Enquiry.findByIdAndDelete(_id)
}