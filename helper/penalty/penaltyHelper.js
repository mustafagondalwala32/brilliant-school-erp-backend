const Penalty = require('../../models/penalty')
module.exports = {    
    savePenalty: async(data,_id) => _id != null ? await Penalty.findByIdAndUpdate(_id,data, {new: true}) : await Penalty.create(data),
    getTotalPenalty: async(query) => await Penalty.find(query).countDocuments(),
    getPenaltyData: async(query,skip,limit) => await Penalty.find(query).sort('-updatedAt').skip(skip).limit(limit),
    deletePenaltyFromDB: async(_id) => await Penalty.findByIdAndDelete(_id)
}