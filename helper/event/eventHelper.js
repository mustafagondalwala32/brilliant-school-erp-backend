const Event = require('../../models/event')
module.exports = {    
    saveEvent: async(data,_id) => _id != null ? await Event.findByIdAndUpdate(_id,data, {new: true}) : await Event.create(data),
    getTotalEvent: async(query) => await Event.find(query).countDocuments(),
    getEventData: async(query,skip,limit) => await Event.find(query).sort('-updatedAt').skip(skip).limit(limit),
    deleteEventFromDB: async(_id) => await Event.findByIdAndDelete(_id)
}