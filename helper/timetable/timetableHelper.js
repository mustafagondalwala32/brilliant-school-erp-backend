const TimeTable = require('../../models/classTimeTable')
const TeacherTimeTable = require('../../models/teacherTimeTable')
module.exports = {    
    saveTimeTable: async(data,_id = null) => {
        if(_id != null){
            return await TimeTable.findByIdAndUpdate(_id,data, {new: true})
        }else{
            return await TimeTable.create(data)
        }
    },
    saveTeacherTimeTable: async(data, _id = null) => {
        if(_id != null){
            return await TeacherTimeTable.findByIdAndUpdate(_id,data, {new: true})
        }else{
            return await TeacherTimeTable.create(data)
        }
    },
    getTimeTableTotal: async(query) => {
        return await TimeTable.find(query).countDocuments()
    },
    getTimeTableData: async(query,skip,limit) => {
        return await TimeTable.find(query).sort('-updatedAt').skip(skip).limit(limit)
    },
    deleteTimeTableFromDB: async(_id) => {
        return await TimeTable.findByIdAndDelete(_id)
    },
    getTeacherTimeTableTotal: async(query) => {
        return await TeacherTimeTable.find(query).countDocuments()
    },
    getTeacherTimeTableData: async(query,skip,limit) => {
        return await TeacherTimeTable.find(query).sort('-updatedAt').skip(skip).limit(limit)
    },
    getIndividualTimetable: async(timetableId) => await TimeTable.findById(timetableId)
        .populate('timetableStructure.monday.periodId')
        .populate('timetableStructure.monday.subjectId')
        .populate('timetableStructure.monday.teacherId')
        .populate('timetableStructure.tuesday.periodId')
        .populate('timetableStructure.tuesday.subjectId')
        .populate('timetableStructure.tuesday.teacherId')
        .populate('timetableStructure.wednesday.periodId')
        .populate('timetableStructure.wednesday.subjectId')
        .populate('timetableStructure.wednesday.teacherId')
        .populate('timetableStructure.thursday.periodId')
        .populate('timetableStructure.thursday.subjectId')
        .populate('timetableStructure.thursday.teacherId')
        .populate('timetableStructure.friday.periodId')
        .populate('timetableStructure.friday.subjectId')
        .populate('timetableStructure.friday.teacherId')
        .populate('timetableStructure.saturday.periodId')
        .populate('timetableStructure.saturday.subjectId')
        .populate('timetableStructure.saturday.teacherId')
}