const Bus = require('../../models/bus')
const BusRoute = require('../../models/bus_route')
module.exports = {    
    saveBus: async(data,_id) => _id != null ? await Bus.findByIdAndUpdate(_id,data, {new: true}) : await Bus.create(data),
    getTotalBus: async(query) => await Bus.find(query).countDocuments(),
    getBusData: async(query,skip,limit) => await Bus.find(query).sort('-updatedAt').skip(skip).limit(limit),
    deleteBusFromDB: async(_id) => await Bus.findByIdAndDelete(_id),
    getTotalRoutes: async(query) => await BusRoute.find(query).countDocuments(),
    saveBusRoute:async(data,_id) => _id != null ? await BusRoute.findByIdAndUpdate(_id,data, {new: true}) : await BusRoute.create(data),
    deleteRoute:async(_id) => await BusRoute.findByIdAndDelete(_id),
    getBusRouteData: async(query,skip,limit) => await BusRoute.find(query).sort('-updatedAt').populate('assigned_bus').skip(skip).limit(limit),
    saveBusStop: async(data,_id) => await BusRoute.findByIdAndUpdate(_id, { '$addToSet': { 'stops': data } },{new:true}),
    deleteBusStop: async(stop_id,_id) => await BusRoute.findByIdAndUpdate(_id,   { $pull: {'stops': { _id : stop_id } } } ,{new:true})
}