const ExamType = require('../../models/examType')
module.exports = {    
    saveExamType: async(data,_id) => _id != null ? await ExamType.findByIdAndUpdate(_id,data, {new: true}) : await ExamType.create(data),
    getTotalExamType: async(query) => await ExamType.find(query).countDocuments(),
    getExamTypeData: async(query,skip,limit) => await ExamType.find(query).sort('-updatedAt').skip(skip).limit(limit),
    deleteExamTypeFromDB: async(_id) => await ExamType.findByIdAndDelete(_id)
}