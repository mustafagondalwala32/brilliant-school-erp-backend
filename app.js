#!/usr/bin/env node

require('dotenv').config();
const express = require("express");
const helmet = require("helmet");
const path = require("path");
const cookieParser = require("cookie-parser");

var multipart = require('connect-multiparty');
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const morgan = require("morgan")
const moment = require("moment");
const chalk  = require("chalk");
const faker  = require('faker');  


const { MongoManager } = require("./db");

const indexRouter = require("./routes/index");
const { fake } = require('faker');



require("./globalFunctions");


const mongoManager = new MongoManager({
  useNewUrlParser: true,
  useFindAndModify: false,
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
});
mongoManager.connect();




app.use(express.urlencoded({ extended: true }));
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(multipart());
app.use(morgan(function (tokens, req, res) {
  let method = tokens.method(req, res);
  let url = tokens.url(req, res);
  let resptime = `${tokens['response-time'](req, res)} ms`;
  let time = moment().utcOffset(330).format('DD-MM-YYYY, HH:mm:ss A');
  return ` ${chalk.gray(time)}   ${chalk.bold.greenBright(method)} ${chalk.yellowBright(url)}  ${chalk.yellowBright(resptime)} ${chalk.redBright('api-request')}`;
}))





app.use("/api", indexRouter);
app.use("*", function (req, res) {
  res.status(404).json({ success: false, message: "Invalid route!"});
});


const StudentDocument = require('./models/studentDocument')
const Student = require('./models/student')
const User = require('./models/user')
const Class = require('./models/class')

asyncFunctionForBulk = async(i,classId,schoolId,yearId) => {
  var all_class = await Class.findById(classId)
  if(all_class == null){
      TE("Cannot find class please contact the admin")
  }
  all_class.currentRollNo += 1;
  var rollNo = all_class.rollNoString+all_class.currentRollNo
  const newStudentDocument = await StudentDocument.create({
      fatherOccupation:faker.random.word(100),motherOccupation:faker.random.word(100),guardianOccupation:faker.random.word(100),fatherQualifiedName:faker.random.word(100),motherQualifiedName:faker.random.word(100),student_photo:faker.random.word(100),father_photo:faker.random.word(100),mother_photo:faker.random.word(100),studentAadharCardNo:faker.random.word(100),fatherAadharCardNo:faker.random.word(100),fatherBankName:faker.random.word(100),fatherBankNameNo:faker.random.word(100),studentBankName:faker.random.word(100),studentBankNo:faker.random.word(100),last_marksheet:faker.random.word(100),transferCertificate:faker.random.word(100),incomeCertificate:faker.random.word(100),castCertificate:faker.random.word(100),dobCertificate:faker.random.word(100),studentAadharCard:faker.random.word(100),fatherAadharCard:faker.random.word(100)
  })
  var studentName = faker.name.firstName()
  const newStudentUser = await User({
      name:studentName,
      loginText:rollNo,
      userType:5,
      schoolId,
      yearId,
  })
  newStudentUser.setPassword(rollNo)
  await newStudentUser.save();

  const newStudent = await Student.create({
      schoolId,yearId,classId,rollNo,studentName,fatherName:faker.name.firstName(),motherName:faker.name.firstName(),lastName:faker.name.firstName(),guardianName:faker.name.firstName(),handicapped:0,parentContactNo1:faker.phone.phoneNumber(),parentContactNo2:faker.phone.phoneNumber(),parentEmail:faker.internet.email(),dob:faker.date.recent(),age:10,gender:1,address:faker.address.streetAddress(),block:faker.address.streetAddress(),district:faker.address.streetAddress(),state:faker.address.streetAddress(),pincode:faker.address.streetAddress(),religion:1,caste:1,student_photo:faker.address.streetAddress(),documentId:newStudentDocument._id,userId:newStudentUser._id
  })

  await all_class.save();

  var parentPhone = faker.phone.phoneNumber()
  const newParentUser = await User({
      name:faker.name.firstName(),
      loginText:parentPhone,
      userType:4,
      schoolId,
      yearId
  })
  newParentUser.setPassword(parentPhone)
  await newParentUser.save();
}


seedMongdb = async() => {
    var classId = "605e9c20329883465c8c5e61";
    var schoolId = "605d634598074a30dc3b9e71";
    var yearId = 1;
    for(var i = 0;i<500;i++){
      console.log(i)
      await asyncFunctionForBulk(i,classId,schoolId,yearId);
  }
}


//seedMongdb()

module.exports = app;
