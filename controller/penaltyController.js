const {
    createPenalty,
    updatePenalty,
    deletePenalty,
    getAllPenalty
  } = require("../services/penalty/penaltyService")
    
module.exports = {    
  
    createPenalty: async(req,res) => {
      try {
        ReS(res,await createPenalty(req),200)
      } catch (error) {
        ReE(res, error, 400, "PenaltyController Controller >>> createPenalty method");
      }
    },
    updatePenalty: async(req, res) => {
        try {
            ReS(res,await updatePenalty(req),200)
        } catch (error) {
            ReE(res, error, 400, "PenaltyController Controller >>> updatePenalty method");
        }
    },
    deletePenalty: async(req, res) => {
        try {
            ReS(res,await deletePenalty(req),200)
        } catch (error) {
            ReE(res, error, 400, "PenaltyController Controller >>> deletePenalty method");
        }
    },
    getAllPenalty: async(req, res) => {
        try {
            ReS(res,await getAllPenalty(req),200)
        } catch (error) {
            ReE(res, error, 400, "PenaltyController Controller >>> getAllPenalty method");
        }
    }
}