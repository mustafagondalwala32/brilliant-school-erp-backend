const {
  addFileToS3,
  removeFileFromS3
} = require("../services/upload/uploadService")
    
module.exports = {    
  
  addFileToS3: async(req,res) => {
      try {
        ReS(res,await addFileToS3(req),200)
      } catch (error) {
        ReE(res, error, 400, "uploadController Controller >>> addFileToS3 method");
      }
  },
  removeFileFromS3: async(req, res) => {
    try {
      ReS(res,await removeFileFromS3(req),200)
    } catch (error) {
      ReE(res, error, 400, "uploadController Controller >>> removeFileFromS3 method");
    }
  }
}