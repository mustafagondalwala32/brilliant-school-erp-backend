const {
    createEnquiry,
    updateEnquiry,
    deleteEnquiry,
    getAllEnquiry
  } = require("../services/enquiry/enquiryService")
    
module.exports = {    
  
    createEnquiry: async(req,res) => {
      try {
        ReS(res,await createEnquiry(req),200)
      } catch (error) {
        ReE(res, error, 400, "holidayController Controller >>> createEnquiry method");
      }
    },
    updateEnquiry: async(req, res) => {
        try {
            ReS(res,await updateEnquiry(req),200)
        } catch (error) {
            ReE(res, error, 400, "holidayController Controller >>> updateEnquiry method");
        }
    },
    deleteEnquiry: async(req, res) => {
        try {
            ReS(res,await deleteEnquiry(req),200)
        } catch (error) {
            ReE(res, error, 400, "holidayController Controller >>> deleteEnquiry method");
        }
    },
    getAllEnquiry: async(req, res) => {
        try {
            ReS(res,await getAllEnquiry(req),200)
        } catch (error) {
            ReE(res, error, 400, "holidayController Controller >>> getAllEnquiry method");
        }
    }
}