const {
    createHoliday,
    updateHoliday,
    deleteHoliday,
    getAllHoliday
  } = require("../services/holiday/holidayService")
    
module.exports = {    
  
    createHoliday: async(req,res) => {
      try {
        ReS(res,await createHoliday(req),200)
      } catch (error) {
        ReE(res, error, 400, "holidayController Controller >>> createHoliday method");
      }
    },
    updateHoliday: async(req, res) => {
        try {
            ReS(res,await updateHoliday(req),200)
        } catch (error) {
            ReE(res, error, 400, "holidayController Controller >>> updateHoliday method");
        }
    },
    deleteHoliday: async(req, res) => {
        try {
            ReS(res,await deleteHoliday(req),200)
        } catch (error) {
            ReE(res, error, 400, "holidayController Controller >>> deleteHoliday method");
        }
    },
    getAllHoliday: async(req, res) => {
        try {
            ReS(res,await getAllHoliday(req),200)
        } catch (error) {
            ReE(res, error, 400, "holidayController Controller >>> getAllHoliday method");
        }
    }
}