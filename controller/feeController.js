const {
    createNewFeeCategory,
    deleteFeeCategory,
    updateFeeCategory,
    getAllFeeCategory,
    createNewFeeInstallment,
    getAllFeeInstallment,
    updateFeeInstallment,
    deleteFeeInstallment,
    updateClassWiseFeeInstallment,
    getClassWiseFeeInstallment,
    addFeeToStudent,
    updateStudentFee
  } = require("../services/fee/feeService")
    
module.exports = {    
  
    createNewFeeCategory: async(req,res) => {
      try {
        ReS(res,await createNewFeeCategory(req),200)
      } catch (error) {
        ReE(res, error, 400, "feeController Controller >>> createNewFeeCategory method");
      }
    },
    updateFeeCategory: async(req, res) => {
      try {
        ReS(res,await updateFeeCategory(req),200)
      } catch (error) {
        ReE(res, error, 400, "feeController Controller >>> updateFeeCategory method");
      }
    },
    deleteFeeCategory: async(req, res) => {
      try {
        ReS(res,await deleteFeeCategory(req),200)
      } catch (error) {
        ReE(res, error, 400, "feeController Controller >>> deleteFeeCategory method");
      }
    },
    getAllFeeCategory: async(req, res) => {
      try {
        ReS(res,await getAllFeeCategory(req),200)
      } catch (error) {
        ReE(res, error, 400, "feeController Controller >>> getAllFeeCategory method");
      }
    },
    createNewFeeInstallment: async(req, res) => {
      try {
        ReS(res,await createNewFeeInstallment(req),200)
      } catch (error) {
        ReE(res, error, 400, "feeController Controller >>> createNewFeeInstallment method");
      }
    },
    
    getAllFeeInstallment: async(req, res) => {
      try {
        ReS(res,await getAllFeeInstallment(req),200)
      } catch (error) {
        ReE(res, error, 400, "feeController Controller >>> getAllFeeInstallment method");
      }
    },
    
    updateFeeInstallment: async(req, res) => {
      try {
        ReS(res,await updateFeeInstallment(req),200)
      } catch (error) {
        ReE(res, error, 400, "feeController Controller >>> updateFeeInstallment method");
      }
    },
    
    deleteFeeInstallment: async(req, res) => {
      try {
        ReS(res,await deleteFeeInstallment(req),200)
      } catch (error) {
        ReE(res, error, 400, "feeController Controller >>> deleteFeeInstallment method");
      }
    },
    updateClassWiseFeeInstallment: async(req, res) => {
      try {
        ReS(res,await updateClassWiseFeeInstallment(req),200)
      } catch (error) {
        ReE(res, error, 400, "feeController Controller >>> updateClassWiseFeeInstallment method");
      }
    },
    getClassWiseFeeInstallment: async(req, res) => {
      try {
        ReS(res,await getClassWiseFeeInstallment(req),200)
      } catch (error) {
        ReE(res, error, 400, "feeController Controller >>> getClassWiseFeeInstallment method");
      }
    },
    addFeeToStudent: async(req, res) => {
      try {
        ReS(res,await addFeeToStudent(req),200)
      } catch (error) {
        ReE(res, error, 400, "feeController Controller >>> addFeeToStudent method");
      }
    },
    updateStudentFee: async(req, res) => {
      try {
        ReS(res,await updateStudentFee(req),200)
      } catch (error) {
        ReE(res, error, 400, "feeController Controller >>> updateStudentFee method");
      }
    }
}