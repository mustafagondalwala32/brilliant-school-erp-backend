const {
    createNewClass,
    deleteClass,
    fetchAllClass,
    updateClass,
    createNewSection,
    assignTeacherToClass,
    assignSubjectToClass
  } = require("../services/class/classService")
    
module.exports = {    
  
    createNewClass: async(req,res) => {
      try {
        ReS(res,await createNewClass(req),200)
      } catch (error) {
        ReE(res, error, 400, "classController Controller >>> createNewClass method");
      }
    },
    deleteClass: async(req,res) => {
      try {
        ReS(res,await deleteClass(req),200)
      } catch (error) {
        ReE(res, error, 400, "classController Controller >>> deleteClass method");
      }
    },
    fetchAllClass: async(req,res) => {
      try {
        ReS(res,await fetchAllClass(req),200)
      } catch (error) {
        ReE(res, error, 400, "classController Controller >>> fetchAllClass method");
      }
    },
    updateClass: async(req,res) => {
      try {
        ReS(res,await updateClass(req),200)
      } catch (error) {
        ReE(res, error, 400, "classController Controller >>> updateClass method");
      }
    },
    createNewSection: async(req,res) => {
      try {
        ReS(res,await createNewSection(req),200)
      } catch (error) {
        ReE(res, error, 400, "classController Controller >>> createNewSection method");
      }
    },
    assignTeacherToClass: async(req, res) => {
      try {
        ReS(res,await assignTeacherToClass(req),200)
      } catch (error) {
        ReE(res, error, 400, "classController Controller >>> assignTeacherToClass method");
      }
    },
    assignSubjectToClass:async (req, res) => {
      try {
        ReS(res,await assignSubjectToClass(req),200)
      } catch (error) {
        ReE(res, error, 400, "classController Controller >>> assignSubjectToClass method");
      }
    }
}