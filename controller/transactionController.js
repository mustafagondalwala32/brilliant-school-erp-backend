const {
    createTransaction,
    updateTransaction,
    deleteTransaction,
    getAllTransaction
  } = require("../services/transaction/transactionService")
    
module.exports = {    
  
    createTransaction: async(req,res) => {
      try {
        ReS(res,await createTransaction(req),200)
      } catch (error) {
        ReE(res, error, 400, "holidayController Controller >>> createTransaction method");
      }
    },
    updateTransaction: async(req, res) => {
        try {
            ReS(res,await updateTransaction(req),200)
        } catch (error) {
            ReE(res, error, 400, "holidayController Controller >>> updateTransaction method");
        }
    },
    deleteTransaction: async(req, res) => {
        try {
            ReS(res,await deleteTransaction(req),200)
        } catch (error) {
            ReE(res, error, 400, "holidayController Controller >>> deleteTransaction method");
        }
    },
    getAllTransaction: async(req, res) => {
        try {
            ReS(res,await getAllTransaction(req),200)
        } catch (error) {
            ReE(res, error, 400, "holidayController Controller >>> getAllTransaction method");
        }
    }
}