const {
  createTimetable,
  updateTimetable,
  getAllTimetable,
  deleteTimetable,
  getIndividualTimetable,
  createTeacherTimetable,
  updateTeacherTimetable,
  getAllTeacherTimetable
} = require("../services/timetable/timetableService")
  
module.exports = {    

  createTimetable: async(req,res) => {
    try {
      ReS(res,await createTimetable(req),200)
    } catch (error) {
      ReE(res, error, 400, "TimetableController Controller >>> createTimetable method");
    }
  },
  updateTimetable: async(req, res) => {
    try {
      ReS(res,await updateTimetable(req),200)
    } catch (error) {
      ReE(res, error, 400, "TimetableController Controller >>> updateTimetable method");
    }
  },
  getAllTimetable: async(req, res) => {
    try {
      ReS(res,await getAllTimetable(req),200)
    } catch (error) {
      ReE(res, error, 400, "TimetableController Controller >>> getAllTimetable method");
    }
  },
  deleteTimetable: async(req, res) => {
    try {
      ReS(res,await deleteTimetable(req),200)
    } catch (error) {
      ReE(res, error, 400, "TimetableController Controller >>> deleteTimetable method");
    }
  },
  getIndividualTimetable: async(req, res) => {
    try {
      ReS(res,await getIndividualTimetable(req),200)
    } catch (error) {
      ReE(res, error, 400, "TimetableController Controller >>> getIndividualTimetable method");
    }
  },
  createTeacherTimetable: async(req, res) => {
    try {
      ReS(res,await createTeacherTimetable(req),200)
    } catch (error) {
      ReE(res, error, 400, "TimetableController Controller >>> createTeacherTimetable method");
    }
  },
  updateTeacherTimetable: async(req, res) => {
    try {
      ReS(res,await updateTeacherTimetable(req),200)
    } catch (error) {
      ReE(res, error, 400, "TimetableController Controller >>> updateTeacherTimetable method");
    }
  },
  getAllTeacherTimetable: async(req, res) => {
    try {
      ReS(res,await getAllTeacherTimetable(req),200)
    } catch (error) {
      ReE(res, error, 400, "TimetableController Controller >>> getAllTeacherTimetable method");
    }
  }
}