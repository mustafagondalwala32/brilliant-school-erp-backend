const {
    createExamType,
    updateExamType,
    deleteExamType,
    getAllExamType
  } = require("../services/examType/examTypeService")
    
module.exports = {    
  
    createExamType: async(req,res) => {
      try {
        ReS(res,await createExamType(req),200)
      } catch (error) {
        ReE(res, error, 400, "holidayController Controller >>> createExamType method");
      }
    },
    updateExamType: async(req, res) => {
        try {
            ReS(res,await updateExamType(req),200)
        } catch (error) {
            ReE(res, error, 400, "holidayController Controller >>> updateExamType method");
        }
    },
    deleteExamType: async(req, res) => {
        try {
            ReS(res,await deleteExamType(req),200)
        } catch (error) {
            ReE(res, error, 400, "holidayController Controller >>> deleteExamType method");
        }
    },
    getAllExamType: async(req, res) => {
        try {
            ReS(res,await getAllExamType(req),200)
        } catch (error) {
            ReE(res, error, 400, "holidayController Controller >>> getAllExamType method");
        }
    }
}