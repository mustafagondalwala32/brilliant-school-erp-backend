const {
    createNewRegisterStudent,
    fetchAllRegisterStudent,
    updateRegisterStudent
  } = require("../services/registerStudent/registerStudentService")
    
module.exports = {    
  
    createNewRegisterStudent: async(req,res) => {
      try {
        ReS(res,await createNewRegisterStudent(req),200)
      } catch (error) {
        ReE(res, error, 400, "schoolController Controller >>> createNewRegisterStudent method");
      }
    },
    fetchAllRegisterStudent: async(req, res) => {
      try {
        ReS(res,await fetchAllRegisterStudent(req),200)
      } catch (error) {
        ReE(res, error, 400, "schoolController Controller >>> fetchAllRegisterStudent method");
      }
    },
    updateRegisterStudent: async(req, res) => {
      try {
        ReS(res,await updateRegisterStudent(req),200)
      } catch (error) {
        ReE(res, error, 400, "schoolController Controller >>> updateRegisterStudent method");
      }
    }
}