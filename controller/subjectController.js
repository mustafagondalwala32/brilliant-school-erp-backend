const {
    createSubject,
    updateSubject,
    getAllSubject,
    deleteSubject
  } = require("../services/subject/subjectService")
    
module.exports = {    
  
    createSubject: async(req,res) => {
      try {
        ReS(res,await createSubject(req),200)
      } catch (error) {
        ReE(res, error, 400, "subjectController Controller >>> createSubject method");
      }
    },
    updateSubject: async(req, res) => {
      try {
        ReS(res,await updateSubject(req),200)
      } catch (error) {
        ReE(res, error, 400, "subjectController Controller >>> updateSubject method");
      }
    },
    getAllSubject: async(req, res) => {
      try {
        ReS(res,await getAllSubject(req),200)
      } catch (error) {
        ReE(res, error, 400, "subjectController Controller >>> getAllSubject method");
      }
    },
    deleteSubject: async(req, res) => {
      try {
        ReS(res,await deleteSubject(req),200)
      } catch (error) {
        ReE(res, error, 400, "subjectController Controller >>> deleteSubject method");
      }
    }
}