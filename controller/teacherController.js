const {
    createNewTeacher,
    getAllTeacher,
    getTeacher,
    searchTeacher
  } = require("../services/teacher/teacherService")
  
  module.exports = {    
  
   createNewTeacher:async(req,res) => {
        try {
            ReS(res,await createNewTeacher(req),200)
        } catch (error) {
          ReE(res, error, 400, "teacherController Controller >>> createNewTeacher method");
        }
    },
    getAllTeacher:async(req,res) => {
      try {
        ReS(res,await getAllTeacher(req),200)
      } catch (error) {
        ReE(res, error, 400, "teacherController Controller >>> getAllTeacher method");
      }
    },
    getTeacher:async(req,res) => {
      try {
        ReS(res,await getTeacher(req),200)
      } catch (error) {
        ReE(res, error, 400, "teacherController Controller >>> getTeacher method");
      }
    },
    searchTeacher:async(req,res) => {
      try {
        ReS(res,await searchTeacher(req),200)
      } catch (error) {
        ReE(res, error, 400, "teacherController Controller >>> searchTeacher method");
      }
    }
  }