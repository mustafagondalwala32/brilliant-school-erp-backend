const {
    createNewPeriod,
    updatePeriod,
    getAllPeriods
  } = require("../services/period/periodService")
    
module.exports = {    
  
    createNewPeriod: async(req,res) => {
      try {
        ReS(res,await createNewPeriod(req),200)
      } catch (error) {
        ReE(res, error, 400, "periodController Controller >>> createNewPeriod method");
      }
    },
    updatePeriod: async(req, res) => {
      try {
        ReS(res,await updatePeriod(req),200)
      } catch (error) {
        ReE(res, error, 400, "periodController Controller >>>  updatePeriod method");
      }
    },
    getAllPeriods: async(req, res) => {
      try {
        ReS(res,await getAllPeriods(req),200)
      } catch (error) {
        ReE(res, error, 400, "periodController Controller >>>  getAllPeriods method");
      }
    },
    deletePeriod: async(req, res) => {
      try {
        ReS(res,await deletePeriod(req),200)
      } catch (error) {
        ReE(res, error, 400, "periodController Controller >>>  deletePeriod method");
      }
    }
}