const {
    createStaff,
    updateStaff,
    deleteStaff,
    getAllStaff
  } = require("../services/staff/staffService")
    
module.exports = {    
  
    createStaff: async(req,res) => {
      try {
        ReS(res,await createStaff(req),200)
      } catch (error) {
        ReE(res, error, 400, "StaffController Controller >>> createStaff method");
      }
    },
    updateStaff: async(req, res) => {
        try {
            ReS(res,await updateStaff(req),200)
        } catch (error) {
            ReE(res, error, 400, "StaffController Controller >>> updateStaff method");
        }
    },
    deleteStaff: async(req, res) => {
        try {
            ReS(res,await deleteStaff(req),200)
        } catch (error) {
            ReE(res, error, 400, "StaffController Controller >>> deleteStaff method");
        }
    },
    getAllStaff: async(req, res) => {
        try {
            ReS(res,await getAllStaff(req),200)
        } catch (error) {
            ReE(res, error, 400, "StaffController Controller >>> getAllStaff method");
        }
    }
}