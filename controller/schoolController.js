const {
  createSchool,
  updateSchool,
  deleteSchool,
  getAllSchool,
  getIndividualSchool
} = require("../services/school/schoolService")
  
module.exports = {    

  createSchool: async(req,res) => {
    try {
      ReS(res,await createSchool(req),200)
    } catch (error) {
      ReE(res, error, 400, "schoolController Controller >>> show createSchool method");
    }
  },
  updateSchool: async(req, res) => {
    try {
      ReS(res,await updateSchool(req),200)
    } catch (error) {
      ReE(res, error, 400, "schoolController Controller >>> updateSchool method");
    }
  },
  deleteSchool: async(req, res) => {
    try {
      ReS(res,await deleteSchool(req),200)
    } catch (error) {
      ReE(res, error, 400, "schoolController Controller >>> deleteSchool method");
    }
  },
  getAllSchool: async(req, res) => {
    try {
      ReS(res,await getAllSchool(req),200)
    } catch (error) {
      ReE(res, error, 400, "schoolController Controller >>> getAllSchool method");
    }
  },
  getIndividualSchool: async(req, res) => {
    try {
      ReS(res,await getIndividualSchool(req),200)
    } catch (error) {
      ReE(res, error, 400, "schoolController Controller >>> getIndividualSchool method");
    }
  },
}