const {
    createBus,
    updateBus,
    deleteBus,
    getAllBus,
    createBusRoute,
    updateBusRoute,
    deleteBusRoute,
    getAllBusRoute,
    createBusRouteStop,
    deleteBusRouteStop,
  } = require("../services/bus/busService")
    
module.exports = {    
  
    createBus: async(req,res) => {
      try {
        ReS(res,await createBus(req),200)
      } catch (error) {
        ReE(res, error, 400, "busController Controller >>> createBus method");
      }
    },
    updateBus: async(req, res) => {
        try {
            ReS(res,await updateBus(req),200)
        } catch (error) {
            ReE(res, error, 400, "busController Controller >>> updateBus method");
        }
    },
    deleteBus: async(req, res) => {
        try {
            ReS(res,await deleteBus(req),200)
        } catch (error) {
            ReE(res, error, 400, "busController Controller >>> deleteBus method");
        }
    },
    getAllBus: async(req, res) => {
        try {
            ReS(res,await getAllBus(req),200)
        } catch (error) {
            ReE(res, error, 400, "busController Controller >>> getAllBus method");
        }
    },
    createBusRoute: async(req, res) => {
        try {
            ReS(res,await createBusRoute(req),200)
        } catch (error) {
            ReE(res, error, 400, "busController Controller >>> createBusRoute method");
        }
    },
    updateBusRoute: async(req, res) => {
        try {
            ReS(res,await updateBusRoute(req),200)
        } catch (error) {
            ReE(res, error, 400, "busController Controller >>> updateBusRoute method");
        }
    },
    deleteBusRoute: async(req, res) => {
        try {
            ReS(res,await deleteBusRoute(req),200)
        } catch (error) {
            ReE(res, error, 400, "busController Controller >>> deleteBusRoute method");
        }
    },
    getAllBusRoute: async(req, res) => {
        try {
            ReS(res,await getAllBusRoute(req),200)
        } catch (error) {
            ReE(res, error, 400, "busController Controller >>> getAllBusRoute method");
        }
    },
    createBusRouteStop: async(req, res) => {
        try {
            ReS(res,await createBusRouteStop(req),200)
        } catch (error) {
            ReE(res, error, 400, "busController Controller >>> createBusRouteStop method");
        }
    },
    deleteBusRouteStop: async(req, res) => {
        try {
            ReS(res,await deleteBusRouteStop(req),200)
        } catch (error) {
            ReE(res, error, 400, "busController Controller >>> deleteBusRouteStop method");
        }
    }
}