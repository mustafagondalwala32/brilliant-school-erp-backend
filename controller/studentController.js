const {
    createNewStudent,
    fetchAllStudent,
    updateStudent,
    addUpdateStudentRollNo,
    getSearchStudent
  } = require("../services/student/studentService")
    
module.exports = {    
  
    createNewStudent: async(req,res) => {
      try {
        ReS(res,await createNewStudent(req),200)
      } catch (error) {
        ReE(res, error, 400, "studentController Controller >>> createNewStudent method");
      }
    },
    fetchAllStudent: async(req, res) => {
      try {
        ReS(res,await fetchAllStudent(req),200)
      } catch (error) {
        ReE(res, error, 400, "studentController Controller >>> fetchAllStudent method");
      }
    },
    updateStudent: async(req, res) => {
      try {
        ReS(res,await updateStudent(req),200)
      } catch (error) {
        ReE(res, error, 400, "studentController Controller >>> updateStudent method");
      }
    },
    addUpdateStudentRollNo: async(req, res) => {
      try {
        ReS(res,await addUpdateStudentRollNo(req),200)
      } catch (error) {
        ReE(res, error, 400, "studentController Controller >>> addUpdateStudentRollNo method");
      }
    },
    getSearchStudent: async(req, res) => {
      try {
        ReS(res,await getSearchStudent(req),200)
      } catch (error) {
        ReE(res, error, 400, "studentController Controller >>> getSearchStudent method");
      }
    }
}