const {
    createSchoolAdmin,
    updateSchoolAdmin
  } = require("../services/schoolAdmin/schoolAdminService")
    
module.exports = {    
  
    createSchoolAdmin: async(req,res) => {
      try {
        ReS(res,await createSchoolAdmin(req),200)
      } catch (error) {
        ReE(res, error, 400, "schoolController Controller >>> show createSchoolAdmin method");
      }
    },
    updateSchoolAdmin: async(req,res) => {
      try {
        ReS(res,await updateSchoolAdmin(req),200)
      } catch (error) {
        ReE(res, error, 400, "schoolController Controller >>> show updateSchoolAdmin method");
      }
    }
}