const {
    createEvent,
    updateEvent,
    deleteEvent,
    getAllEvent
  } = require("../services/event/eventService")
    
module.exports = {    
  
    createEvent: async(req,res) => {
      try {
        ReS(res,await createEvent(req),200)
      } catch (error) {
        ReE(res, error, 400, "EventController Controller >>> createEvent method");
      }
    },
    updateEvent: async(req, res) => {
        try {
            ReS(res,await updateEvent(req),200)
        } catch (error) {
            ReE(res, error, 400, "EventController Controller >>> updateEvent method");
        }
    },
    deleteEvent: async(req, res) => {
        try {
            ReS(res,await deleteEvent(req),200)
        } catch (error) {
            ReE(res, error, 400, "EventController Controller >>> deleteEvent method");
        }
    },
    getAllEvent: async(req, res) => {
        try {
            ReS(res,await getAllEvent(req),200)
        } catch (error) {
            ReE(res, error, 400, "EventController Controller >>> getAllEvent method");
        }
    }
}